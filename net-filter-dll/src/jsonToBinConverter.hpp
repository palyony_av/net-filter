#ifndef __JSONTOBINARYCONVERTER_HPP__
#define __JSONTOBINARYCONVERTER_HPP__

#include <memory>
#include <nlohmann/json.hpp>
#include <cstdint>

#include "rule.h"

class JsonToBinConverter
{
public:
    static std::unique_ptr<FILTER_CFG> convertToBin(const char * pBuf, size_t bufSize);
    static std::tuple<IN_ADDR, bool> strToIpv4(std::string strIpv4);
private:
    JsonToBinConverter() = default;
};

std::unique_ptr<FILTER_CFG> JsonToBinConverter::convertToBin(const char * pBuf, size_t bufSize)
{
    if (pBuf == nullptr || bufSize == 0)
    {
        return nullptr;
    }

    bool isParseOk = false;
    nlohmann::json jMsg;
    try
    {
        jMsg = nlohmann::json::parse(pBuf);
        isParseOk = true;
    }
    catch (const nlohmann::json::parse_error)
    {
        isParseOk = false;
    }

    if (!isParseOk)
    {
        return nullptr;
    }

    std::unique_ptr<FILTER_CFG> pConfig(new FILTER_CFG());

    pConfig->ruleId = (uint8_t)jMsg["id"].get<int>() & 0xFF;

    /* Set action */
    {
        std::string action = jMsg["action"].get<std::string>();
        if (!action.compare("Block"))
        {
            pConfig->action = IPV4_BLOCK_TRAFFIC;
        } else if (!action.compare("Log"))
        {
            pConfig->action = IPV4_LOG_TRAFFIC;
        } else {
            return nullptr;
        }
    }

    /* Catch all incoming packets */
    pConfig->local.mask = 0;
    pConfig->local.addr.S_un.S_addr = 0;
    pConfig->local.port.range.start = 0;
    pConfig->local.port.range.end = 0xFFFF;

    /* Set remote Ipv4 */
    {
        std::string strRemoteIpv4 = jMsg["ip"].get<std::string>();
        std::tuple<IN_ADDR, bool> remoteIpv4 = strToIpv4(strRemoteIpv4);
        if (std::get<1>(remoteIpv4) == false)
        {
            return nullptr;
        }
        pConfig->remote.mask = 0xFFFFFFFF;
        pConfig->remote.addr = std::get<0>(remoteIpv4);
    }

    /* Set remote port for TCP or UDP */
    {
        pConfig->remote.port.range.start = (uint16_t)jMsg["port"].get<int>() & 0xFFFF;
        pConfig->remote.port.range.end = pConfig->remote.port.range.start;
    }

    /* Set protocol type */
    {
        std::string strProtocolType = jMsg["protocol"].get<std::string>();
        if (!strProtocolType.compare("TCP"))
        {
            pConfig->parameters.bit.tProtocol = TCP_PROTOCOL;
        } else if (!strProtocolType.compare("UDP"))
        {
            pConfig->parameters.bit.tProtocol = UDP_PROTOCOL;
        } else if (!strProtocolType.compare("ICMP"))
        {
            pConfig->parameters.bit.tProtocol = ICMP_PROTOCOL;
        } else if (!strProtocolType.compare("IP"))
        {
            pConfig->parameters.bit.tProtocol = IP_PROTOCOL;
        } else {
            return nullptr;
        }
    }

    /* Set rule direction */
    {
        std::string strRuleDirection = jMsg["type"].get<std::string>();
        if (!strRuleDirection.compare("Incoming"))
        {
            pConfig->parameters.bit.direction = INBOUND_DIRECTION;
        } else if (!strRuleDirection.compare("Outcoming"))
        {
            pConfig->parameters.bit.direction = OUTBOUND_DIRECTION;
        } else {
            return nullptr;
        }
    }

    pConfig->pattern.param.offset = (uint16_t)jMsg["offset"].get<int>() & 0xFFFF;
    pConfig->parameters.bit.useBytePattern = 0;

    /* Set byte pattern */
    {
        size_t byteCount = jMsg["payload"].size();
        pConfig->pattern.param.length = (uint16_t)byteCount & 0xFFFF;
        if (byteCount > 0)
        {
            PFILTER_CFG tempFilter = (PFILTER_CFG)new uint8_t[sizeof(FILTER_CFG) + byteCount];
            std::memcpy(tempFilter, pConfig.get(), sizeof(FILTER_CFG));
            pConfig.reset(tempFilter);

            pConfig->patternData = (uint8_t *)((uint8_t *)pConfig.get() + sizeof(FILTER_CFG));
            pConfig->parameters.bit.useBytePattern = 1;

            uint16_t byteNum = 0;
            for(auto jsonStrByteVal : jMsg["payload"])
            {
                pConfig->patternData[byteNum++] = (uint16_t)jsonStrByteVal.get<int>() & 0xFF;
            }

            pConfig->patternData = (uint8_t *)sizeof(FILTER_CFG);
        }
    }

    return pConfig;
}

inline std::tuple<IN_ADDR, bool> JsonToBinConverter::strToIpv4(std::string strIpv4)
{
    IN_ADDR ipv4Addr = { 0 };
    const char * pStrStart = nullptr;
    char * pStrEnd = nullptr;

    if (strIpv4.size() >= sizeof("255.255.255.255"))
    {
        return std::tuple<IN_ADDR, bool>(ipv4Addr, false);
    }

    pStrStart = strIpv4.c_str();
    ipv4Addr.S_un.S_un_b.s_b1 = (uint8_t)strtoul(pStrStart, &pStrEnd, 10);
    if (*pStrEnd++ != '.')
    {
        return std::tuple<IN_ADDR, bool>(ipv4Addr, false);
    }

    pStrStart = pStrEnd;
    ipv4Addr.S_un.S_un_b.s_b2 = (uint8_t)strtoul(pStrStart, &pStrEnd, 10);
    if (*pStrEnd++ != '.')
    {
        return std::tuple<IN_ADDR, bool>(ipv4Addr, false);
    }

    pStrStart = pStrEnd;
    ipv4Addr.S_un.S_un_b.s_b3 = (uint8_t)strtoul(pStrStart, &pStrEnd, 10);
    if (*pStrEnd++ != '.')
    {
        return std::tuple<IN_ADDR, bool>(ipv4Addr, false);
    }

    pStrStart = pStrEnd;
    ipv4Addr.S_un.S_un_b.s_b4 = (uint8_t)strtoul(pStrStart, &pStrEnd, 10);

    return std::tuple<IN_ADDR, bool>(ipv4Addr, true);
}

#endif // !__JSONTOBINARYCONVERTER_HPP__
