#ifndef LOGMODULECONTEXT_H
#define LOGMODULECONTEXT_H

#include <Windows.h>
#include <memory>
#include <string>

/* Store path for all dll modules here */
#define DLL_LOG_MODULE_NAME "Logger.dll"
#define DLL_LOG_MODULE_PATH "lib/" DLL_LOG_MODULE_NAME

#define FUNC_INSERT_JSON_ENTRY "insertJsonEntry"

typedef void (*func_insertJsonEntry)(const char *, uint32_t);

class LogDllModule
{
public:
    static std::unique_ptr<LogDllModule> build(const char *pLoggerDllPath);

    ~LogDllModule();

    void insertJsonEntry(const char* pJsonEntry, uint32_t jsonEntrySize);
    void insertLogEntry(std::string logEntry);

private:
    HMODULE hLoggerDll;
    func_insertJsonEntry fInsertJsonEntry;

    LogDllModule(HMODULE hOpenedLoggerDll, func_insertJsonEntry fInInsertJsonEntry);
    LogDllModule(const LogDllModule&) = delete;
    LogDllModule& operator=(const LogDllModule&) = delete;

};

#endif // LOGMODULECONTEXT_H
