#ifndef __RULE_H__
#define __RULE_H__

#include <Windows.h>

typedef enum
{
	INBOUND_DIRECTION = 0,
	OUTBOUND_DIRECTION = 1
} TRAFFIC_DIRECTION, * PTRAFFIC_DIRECTION;

typedef enum
{
	IP_PROTOCOL = 0, // Used for all ip packets
	ICMP_PROTOCOL = 1,
	TCP_PROTOCOL = 6,
	UDP_PROTOCOL = 17
} TRANSPORT_PROTOCOL_ENUM, * PTRANSPORT_PROTOCOL_ENUM;

typedef enum
{
	IPV4_LOG_TRAFFIC,
	IPV4_BLOCK_TRAFFIC
} TRAFFIC_ACTION_ENUM, * PTRAFFIC_ACTION_ENUM;

#pragma pack(push, 1)
// TODO: Need to remove unused
typedef struct
{
	union
	{
		UINT32 flag;
		struct
		{
			UINT32 direction : 1;
			UINT32 tProtocol : 8;	// Transport protocol
			UINT32 useBytePattern : 1;
			UINT32 unused : 22;
		} bit;
	};
} PARAMETERS_FLAG, * PPARAMETERS_FLAG;

#define MAX_FLT_CONFIGS 100

typedef union
{
	struct
	{
		UINT16 offset;
		UINT16 length;	// The user can directly set it
	} param;
	UINT32 value;
} PATTERN_PARAM, * PPATTERN_PARAM;

typedef struct
{
	IN_ADDR addr;
	UINT32 mask;
	union
	{
		struct
		{
			UINT16 start;
			UINT16 end;
		} range;
		UINT32 value;
	} port;

} IPV4_RULE, * PIPV4_RULE;

typedef struct
{
	UINT8 ruleId;
	PARAMETERS_FLAG parameters;
	IPV4_RULE remote;
	IPV4_RULE local;

	PATTERN_PARAM pattern;

	PUINT8 patternData;

	TRAFFIC_ACTION_ENUM action;
} FILTER_CFG, * PFILTER_CFG;

typedef struct
{
	UINT8 ruleId;
	UINT8 direction;
} DEL_RULE_INFO, *PDEL_RULE_INFO;

#pragma pack(pop)

typedef void* RULE_HANDLE;

#endif // !__RULE_H__
