#ifndef __WORKER_H__
#define __WORKER_H__

#include <thread>
#include <queue>
#include <string>
#include <mutex>
#include <vector>
#include <cstdint>
#include <condition_variable>

class Worker
{
public:
    using FuncWork = void (*)(void *);
    using FuncInit = void *(*)();

    class Builder;

    ~Worker();
    Worker(const Worker& obj) = delete;
    Worker(Worker&& obj);

    void addNextWorkId(uint32_t workId);
    bool waitForInit();
    void kill();

private:
    class ThreadContext;
    
    std::unique_ptr<ThreadContext> pThreadContext;
    std::thread workThread;
    
    Worker(std::vector<FuncWork> tableFuncWork, FuncInit funcInit, FuncWork funcUninit, FuncWork defaultWork);
    static void work(ThreadContext *pContext);
};

class Worker::Builder
{
public:
    Builder& setVectorFuncWork(std::vector<Worker::FuncWork> inTableFuncWork);
    Builder& setFuncInit(FuncInit inFuncInit);
    Builder& setFuncUnit(FuncWork inFuncUnit);
    Builder& setDefaultWork(FuncWork inFuncWork);
    Worker* build() const;
    
private:
    /* Use vector because the function pointers too small
     * and array reallocation possibly do not invoke */
    std::vector<FuncWork> tableFuncWork;
    FuncInit funcInit = nullptr;
    FuncWork funcUninit = nullptr;
    FuncWork funcDefaultWork = nullptr;
};

class Worker::ThreadContext
{
public:
    enum class State
    {
        init,
        work,
        wait,
        uninit,
        die
    };

    FuncInit init = defaultInit;
    FuncWork uninit = defaultUninit;

    ThreadContext(std::vector<FuncWork> inWorkTable, FuncInit inFuncInit, FuncWork inFuncUninit, FuncWork inDefaultWork);
    ~ThreadContext() = default;

    FuncWork getNextWork();
    void wait();
    void setThreadState(State inState);

    void addNextWorkId(uint32_t workId);
    void kill();
    bool isNeedToLive();
    bool waitForInit();
    State getThreadState();

private:
    FuncWork funcDefaultWork = defaultWork;
    std::vector<FuncWork> workTable;
    State state;
    std::queue<uint32_t> workQueue;
    std::condition_variable cv;
    std::mutex m;
    bool alive = true;

    static void defaultWork(void *);
    static void * defaultInit();
    static void defaultUninit(void *);
};

#endif // !__WORKER_H__
