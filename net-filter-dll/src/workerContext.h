#ifndef __NETFILTERCONTEXT_H__
#define __NETFILTERCONTEXT_H__

#include <memory>

#include "logDllModule.h"
#include "ioDeviceManager.hpp"

class WorkerContext
{
public:
	static WorkerContext* build();
	~WorkerContext();

	IoDeviceManager* getIoDeviceManager();
	LogDllModule* getLogDllModule();

private:
	WorkerContext(std::unique_ptr<IoDeviceManager> pInIoDeviceManager, std::unique_ptr<LogDllModule> pInLogger);

	std::unique_ptr<IoDeviceManager> pIoDeviceManager;
	std::unique_ptr<LogDllModule> pLogger;
};

#endif // !__NETFILTERCONTEXT_H__
