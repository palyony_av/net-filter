#define _CRT_SECURE_NO_WARNINGS

#include <ctime>

#include "logDllModule.h"

std::unique_ptr<LogDllModule> LogDllModule::build(const char* pLoggerDllPath)
{
    HMODULE hLoggerDll = nullptr;
    hLoggerDll = LoadLibraryA(pLoggerDllPath);
    if (hLoggerDll == nullptr)
    {
        return nullptr;
    }

    func_insertJsonEntry fInsertJsonEntry = reinterpret_cast<func_insertJsonEntry>(
        GetProcAddress(hLoggerDll, FUNC_INSERT_JSON_ENTRY)
    );
    if (fInsertJsonEntry == nullptr)
    {
        FreeLibrary(hLoggerDll);
        hLoggerDll = nullptr;
        return nullptr;
    }

    return std::unique_ptr<LogDllModule>(new LogDllModule(hLoggerDll, fInsertJsonEntry));
}

LogDllModule::LogDllModule(HMODULE hOpenedLoggerDll, func_insertJsonEntry fInInsertJsonEntry)
    : hLoggerDll(hOpenedLoggerDll), fInsertJsonEntry(fInInsertJsonEntry)
{
}

LogDllModule::~LogDllModule()
{
    FreeLibrary(this->hLoggerDll);
    this->hLoggerDll = nullptr;
    this->fInsertJsonEntry = nullptr;
}

void LogDllModule::insertJsonEntry(const char *pJsonEntry, uint32_t jsonEntrySize)
{
    this->fInsertJsonEntry(pJsonEntry, jsonEntrySize);
}

void LogDllModule::insertLogEntry(std::string logEntry)
{
    std::string prefix = "{\"id\":0,\"module_id\":6,\"event\":\"";
    std::string postfix = "\",\"date\":\"00-00-0000\",\"time\":\"00:00:00\"}";

    std::time_t t = std::time(nullptr);
    std::tm* now = std::localtime(&t);
    char bufDate[11]; // Because of null character at the end
    char bufTime[9];
    std::strftime(bufDate, 11 * sizeof(char), "%d-%m-%Y", now);
    std::strftime(bufTime, 9 * sizeof(char), "%H:%M:%S", now);
    postfix.replace(static_cast<const size_t>(10), static_cast<const size_t>(10), bufDate);
    postfix.replace(static_cast<const size_t>(30), static_cast<const size_t>(8), bufTime);

    std::string result = prefix + logEntry + postfix;
    this->insertJsonEntry(result.c_str(), static_cast<uint32_t>(result.size()));
}
