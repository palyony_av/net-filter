#include <Windows.h>
#include <thread>
#include <string>
#include <atomic>
#include <vector>
#include <codecvt>

#include "worker.h"
#include "jsonToBinConverter.hpp"
#include "workerContext.h"
#include "dll.h"

#define IO_SYMLINK_NAME L"\\\\.\\PalyonyAVLinkNetFilter"
#define DRIVER_BUFFER_SIZE 2048
#define DRIVER_BUFFER_WCHAR_LEN (DRIVER_BUFFER_SIZE / sizeof(wchar_t))

#define DRIVER_INF_PATH L".\\driver\\net-filter.inf"
#define DRIVER_SERVICE_NAME L"netfilter"

#define AV_NETWORK_REG_KEY L"SOFTWARE\\PalyonyAV\\NetworkProtection"

#define MAX_VALUE_NAME 128
#define MAX_VALUE_SIZE 512

static bool waitWhileServiceStateIs(DWORD dwState, SC_HANDLE hService, SERVICE_STATUS_PROCESS& ssp);
static std::shared_ptr<std::vector<std::string>> getJsonRulesFromRegistry();
static bool installDriverModule();
static bool uninstallDriverModule();

static bool installDriverModule();
static bool uninstallDriverModule();

void* init()
{
    return reinterpret_cast<void*>(WorkerContext::build());
}

void uninit(void* pCtx)
{
    if (pCtx == nullptr)
    {
        return;
    }

    delete static_cast<WorkerContext*>(pCtx);
}

void taskUpdateRuleInfo(void* pCtx)
{
    WorkerContext* pWorkerContext = static_cast<WorkerContext*>(pCtx);

    if (!pWorkerContext->getIoDeviceManager()->clearRules())
    {
        pWorkerContext->getLogDllModule()->insertLogEntry("failed to send request for clearRules");
    }

    auto jsonRules = getJsonRulesFromRegistry();
    for (auto rule : *(jsonRules.get()))
    {
        std::unique_ptr<FILTER_CFG> pCfg = JsonToBinConverter::convertToBin(rule.c_str(), rule.size());
        if (pCfg != nullptr)
        {
            pWorkerContext->getIoDeviceManager()->addRule(pCfg.get());
        }

        pCfg.reset(nullptr);
    }

    pWorkerContext->getLogDllModule()->insertLogEntry("log update is done");
}

void taskDeliverLogs(void* pCtx)
{
    char driverLogsBuffer[DRIVER_BUFFER_SIZE];
    WorkerContext* pWorkerContext = static_cast<WorkerContext*>(pCtx);

    ULONG fillSize = pWorkerContext->getIoDeviceManager()->pullLogs(
        driverLogsBuffer, DRIVER_BUFFER_SIZE - sizeof(char)
    );

    driverLogsBuffer[fillSize] = '\0';

    if (fillSize != 0)
    {
        // TODO: Need to fix freaking logging in driver
        /*
         * We are give freaking log string like: {...}{...}{...}
         * And parser can't parse multiple message so now I decouple a bunch of messages
         * and send by one
         */
        char* pStartMesage = driverLogsBuffer;
        char* pBracket = std::strchr(driverLogsBuffer, '}');
        while (pBracket != nullptr)
        {
            char pPrevChr = pBracket[1];
            pBracket[1] = '\0';

            ULONG strSize = static_cast<ULONG>(&pBracket[1] - pStartMesage);
            pWorkerContext->getLogDllModule()->insertJsonEntry(pStartMesage, strSize);

            pBracket[1] = pPrevChr;
            pStartMesage = &pBracket[1];
            pBracket = std::strchr(pStartMesage, '}');
        }
    }
}

AVHMODULE installModule()
{    
    if (!installDriverModule())
    {
        return nullptr;
    }

    Worker* pWorker = Worker::Builder{}.setFuncInit(init)
                                       .setFuncUnit(uninit)
                                       .setVectorFuncWork(std::vector<Worker::FuncWork>{taskUpdateRuleInfo})
                                       .setDefaultWork(taskDeliverLogs)
                                       .build();

    if (pWorker == nullptr || !pWorker->waitForInit())
    {
        uninstallDriverModule();
        return nullptr;
    }

    return reinterpret_cast<AVHMODULE>(pWorker);
}

unsigned int uninstallModule(AVHMODULE hModule)
{
    if (hModule == nullptr)
    {
        return AV_STATUS_FAIL;
    }

    delete reinterpret_cast<Worker*>(hModule);

    uninstallDriverModule();

    return AV_STATUS_SUCCESS;
}

unsigned int updateModuleData(AVHMODULE hModule)
{
    if (hModule == nullptr)
    {
        return AV_STATUS_FAIL;
    }

    Worker* pWorker = reinterpret_cast<Worker*>(hModule);
    
    /* Execute taskUpdateRuleInfo */
    pWorker->addNextWorkId(0);

    return AV_STATUS_SUCCESS;
}

bool installDriverModule()
{
    SERVICE_STATUS_PROCESS ssp;
    bool result = false;
    SC_HANDLE hSCManager = nullptr;
    SC_HANDLE hService = nullptr;  
    DWORD dwBytesNeeded = 0;

    if (GetFileAttributes(DRIVER_INF_PATH) == INVALID_FILE_ATTRIBUTES && GetLastError() == ERROR_FILE_NOT_FOUND)
    {
        return false;
    }

    /*
     * If ShellExecute succeeds, it returns a value greater than 32.
     * If the function fails, it returns an error value that indicates
     * the cause of the failure
     */

    const ULONG ERROR_BOUNDARY = 32;
#pragma warning(suppress: 4311 4302) // Due to Microsoft's backward compatibility
    ULONG resultShellExecute = reinterpret_cast<ULONG>(ShellExecute(NULL, L"install", DRIVER_INF_PATH, NULL, NULL, SW_SHOWNORMAL));

    if (resultShellExecute <= ERROR_BOUNDARY)
    {
        goto Exit;
    }

    /* Get a handle to the SCM database. */
    hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        result = false;
        goto Exit;
    }

    /* Get a handle to the service. */
    hService = OpenService(hSCManager, DRIVER_SERVICE_NAME, SERVICE_START | SERVICE_QUERY_STATUS);
    if (!hService)
    {
        result = false;
        goto Exit;
    }

    /* Check the status in case the service is not stopped. */
    if (!QueryServiceStatusEx(
        hService,
        SC_STATUS_PROCESS_INFO,
        (LPBYTE)&ssp,
        sizeof(SERVICE_STATUS_PROCESS),
        &dwBytesNeeded))
    {
        result = false;
        goto Exit;
    }

    /* If a stop is pending, wait for it. */
    if (!waitWhileServiceStateIs(SERVICE_STOP_PENDING, hService, ssp))
    {
        result = false;
        goto Exit;
    }

    if (ssp.dwCurrentState != SERVICE_STOPPED)
    {
        result = false;
        goto Exit;
    }

    /* Attempt to start the service. */
    if (!StartService(
        hService,       // handle to service 
        0,              // number of arguments 
        NULL))          // no arguments 
    {
        result = false;
        goto Exit;
    }

    /* Check the status in case the service is not stopped. */
    if (!QueryServiceStatusEx(
        hService,
        SC_STATUS_PROCESS_INFO,
        (LPBYTE)&ssp,
        sizeof(SERVICE_STATUS_PROCESS),
        &dwBytesNeeded))
    {
        result = false;
        goto Exit;
    }

    /* Service start pending, wait for it. */
    if (!waitWhileServiceStateIs(SERVICE_START_PENDING, hService, ssp))
    {
        result = false;
        goto Exit;
    }

    if (ssp.dwCurrentState != SERVICE_RUNNING)
    {
        result = false;
        goto Exit;
    }

    result = true;

Exit:
    if (hService != nullptr)
    {
        CloseServiceHandle(hService);
    }

    if (hSCManager != nullptr)
    {
        CloseServiceHandle(hSCManager);
    }

    return result;
}

bool uninstallDriverModule()
{
    BOOL result = true;
    SERVICE_STATUS_PROCESS ssp;
    DWORD dwBytesNeeded;
    SC_HANDLE hSCManager = nullptr;
    SC_HANDLE hService = nullptr;

    /* Get a handle to the SCM database. */
    hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        result = false;
        goto Exit;
    }

    /* Get a handle to the service. */
    hService = OpenService(hSCManager, DRIVER_SERVICE_NAME, SERVICE_STOP | SERVICE_QUERY_STATUS);
    if (hService == nullptr)
    {
        result = false;
        goto Exit;
    }

    /* Make sure the service is not already stopped. */
    if (!QueryServiceStatusEx(
        hService,
        SC_STATUS_PROCESS_INFO,
        (LPBYTE)&ssp,
        sizeof(SERVICE_STATUS_PROCESS),
        &dwBytesNeeded))
    {
        result = false;
        goto Exit;
    }

    if (ssp.dwCurrentState == SERVICE_STOPPED || ssp.dwCurrentState == SERVICE_STOP_PENDING)
    {
        result = true;
        goto Exit;
    }

    /* Send a stop code to the service. */
    if (!ControlService(
        hService,
        SERVICE_CONTROL_STOP,
        (LPSERVICE_STATUS)&ssp))
    {
        result = false;
        goto Exit;
    }
    /* Service strated to process stopping request. We are do not wait for it */

    if (!waitWhileServiceStateIs(SERVICE_STOP_PENDING, hService, ssp))
    {
        result = false;
        goto Exit;
    }

    result = true;

Exit:
    if (hService != NULL)
    {
        CloseServiceHandle(hService);
    }

    if (hSCManager != NULL)
    {
        CloseServiceHandle(hSCManager);
    }

    return result;
}

bool waitWhileServiceStateIs(DWORD dwState, SC_HANDLE hService, SERVICE_STATUS_PROCESS& ssp)
{
    /* One second in milliseconds */
#define ONE_SECOND 1000

#pragma warning( push )
/*
 * warning C28159: Consider using another function instead
 * Propose to use GetTickCount64[ULONGLONG] instead of GitTickCount[LONG]
 * in SERVICE_STATUS_PROCESS used DWORD so I will use GitTickCount
 */
#pragma warning( disable : 28159 )

    DWORD dwStartTickCount = 0;
    DWORD dwOldCheckPoint = 0;
    bool result = false;

    dwStartTickCount = GetTickCount();
    dwOldCheckPoint = ssp.dwCheckPoint;
    while (ssp.dwCurrentState == dwState)
    {
        DWORD dwWaitTime = 0;
        DWORD dwBytesNeeded = 0;
        /*
         * Do not wait longer than the wait hint. A good interval is
         * one-tenth of the wait hint but not less than 1 second
         * and not more than 10 seconds.
         */
        dwWaitTime = ssp.dwWaitHint / 10;

        if (dwWaitTime < ONE_SECOND)
        {
            dwWaitTime = ONE_SECOND;
        }
        else if (dwWaitTime > ONE_SECOND)
        {
            dwWaitTime = ONE_SECOND;
        }

        Sleep(dwWaitTime);

        if (!QueryServiceStatusEx(
            hService,
            SC_STATUS_PROCESS_INFO,
            (LPBYTE)&ssp,
            sizeof(SERVICE_STATUS_PROCESS),
            &dwBytesNeeded))
        {
            result = false;
            goto Exit;
        }

        if (ssp.dwCheckPoint > dwOldCheckPoint)
        {
            /* Continue to wait and check. */
            dwStartTickCount = GetTickCount();
            dwOldCheckPoint = ssp.dwCheckPoint;
        }
        else if ((GetTickCount() - dwStartTickCount) > ssp.dwWaitHint)
        {
            result = false;
            goto Exit;
        }
    }
    result = true;

Exit:
    return result;

#pragma warning( pop )
}

std::shared_ptr<std::vector<std::string>> getJsonRulesFromRegistry()
{
    auto objects = std::make_shared<std::vector<std::string>>();

    DWORD nValues = 0;

    WCHAR achValue[MAX_VALUE_NAME];
    WCHAR valueBuffer[MAX_VALUE_SIZE];
    LSTATUS retCreateKeyCode = 0;
    LSTATUS retQueryInfoKeyCode = 0;
    HKEY hFilterKey = nullptr;

    /* Used only to convert from wide char */
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    retCreateKeyCode = RegCreateKeyEx(HKEY_LOCAL_MACHINE, AV_NETWORK_REG_KEY, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hFilterKey, NULL);
    if (retCreateKeyCode != ERROR_SUCCESS)
    {
        goto Exit;
    }

    retQueryInfoKeyCode = RegQueryInfoKey(hFilterKey, NULL, NULL, NULL, NULL, NULL, NULL, &nValues, NULL, NULL, NULL, NULL);
    if (retQueryInfoKeyCode != ERROR_SUCCESS || nValues == 0)
    {
        goto Exit;
    }

    for (DWORD i = 0; i < nValues; i++)
    {
        DWORD retCode = ERROR_SUCCESS;

        DWORD cchValue = MAX_VALUE_NAME;
        achValue[0] = L'\0';
        retCode = RegEnumValue(hFilterKey, i, achValue, &cchValue, NULL, NULL, NULL, NULL);

        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        DWORD bufferSize = sizeof(valueBuffer);
        valueBuffer[0] = L'\0';

        retCode = RegGetValue(HKEY_LOCAL_MACHINE, AV_NETWORK_REG_KEY, achValue,
            RRF_RT_REG_SZ, NULL, &valueBuffer, &bufferSize);
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        valueBuffer[0] = L',';
        /* Convert wstring to string and add it to the vector */
        std::wstring result_buffer = L"{\"id\":";
        result_buffer += achValue;
        result_buffer += valueBuffer;
        objects->push_back(converterX.to_bytes(result_buffer));
    }

Exit:
    if (hFilterKey != nullptr)
    {
        RegCloseKey(hFilterKey);
    }

    return objects;
}
