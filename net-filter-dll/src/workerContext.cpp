#include "workerContext.h"

WorkerContext* WorkerContext::build()
{
    std::unique_ptr<IoDeviceManager> pIoDeviceManager = IoDeviceManager::make(IO_SYMLINK_NAME);
    if (pIoDeviceManager == nullptr)
    {
        return nullptr;
    }

    std::unique_ptr<LogDllModule> pLogger = LogDllModule::build(DLL_LOG_MODULE_PATH);
    if (pLogger == nullptr)
    {
        return nullptr;
    }

	return new WorkerContext(std::move(pIoDeviceManager), std::move(pLogger));
}

WorkerContext::~WorkerContext()
{
}

IoDeviceManager* WorkerContext::getIoDeviceManager()
{
    return this->pIoDeviceManager.get();
}

LogDllModule* WorkerContext::getLogDllModule()
{
    return this->pLogger.get();
}

WorkerContext::WorkerContext(std::unique_ptr<IoDeviceManager> pInIoDeviceManager, std::unique_ptr<LogDllModule> pInLogger)
    :pIoDeviceManager(std::move(pInIoDeviceManager)), pLogger(std::move(pInLogger))
{

}
