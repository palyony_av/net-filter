# Net Filter

Network filter driver. It can block, log based on rules.

# To test

Create python server on the host:
```python -m http.server 8000 --bind 0.0.0.0
```

On the target:
```
(New-Object System.Net.WebClient).DownloadFile("http://127.0.0.1/.gitconfig", "C:\Users\sysli\.gitconfig")
```

Warning: The driver must be KMDF with major version 1. Also installation is possible only with help of WdfCoinstaller01011.dll
https://blogs.msdn.microsoft.com/iliast/2008/03/10/why-do-we-need-wdf-coinstallers/
