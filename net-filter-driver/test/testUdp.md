# Requirements:
1. PC or virtual machine with at least Windows 7 x64. On this PC we will run driver. Ncat - the part of the packet Nmap (https://nmap.org/download.html). Let's name it guest.
2. PC or virtual machine with at least Windows with installed Ncat - the part of the packet Nmap (https://nmap.org/download.html). Let's name it host.
3. Connection between host and guest. Assume host has IP 192.168.202.1 and guest - 192.168.202.128.

# Test
1. Run driver on guest and with help of DbgView start to listen debug messages.
2. On host execute in command line (cmd.exe) the following: `ncat -u -l 33096`. The udp server is starts.
3. On guest execute in command line (cmd.exe) the following: `ncat -u 192.168.202.1 33096`. We are connect to udp server. Type something and press **Enter**.
5. Setup filter rules in **HKLM\\SYSTEM\\CurrentControlSet\\Services\\netfilter\\Config\\**.
6. On guest, view debug output in DbgView.
