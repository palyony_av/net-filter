# Requirements:
1. PC or virtual machine with at least Windows 7 x64. On this PC we will run driver. Let's name it guest.
2. PC or virtual machine with at least Windows with installed Python 3.6 or higher. Let's name it host.
3. Connection between host and guest. Assume host has IP 192.168.202.1 and guest - 192.168.202.128.

# Test
1. Run driver on guest and with help of DbgView start to listen debug messages.
2. On host execute in command line (cmd.exe) the following: `python -m http.server --bind 0.0.0.0 --cgi 8000`. The python server starts and was wait for clients and requests.
3. On host create sample text file. Let's name it `.gitconfig.txt`.
4. On guest open Powershell and execute the following command: `(New-Object System.Net.WebClient).DownloadFile("http://192.168.202.1:8000/.gitconfig", "C:\Users\test\Desktop\.gitconfig")`. Note: replace **test** with your username.
5. Setup filter rules in **HKLM\\SYSTEM\\CurrentControlSet\\Services\\netfilter\\Config\\**.
6. On guest, view debug output in DbgView.
