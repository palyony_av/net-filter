To generate GUID:
1. You need to be sure that windows SDK is installed
2. Go to the *C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\ *
3. Run program *guidgen.exe*
4. Choose *DEFINE_GUID(...)* radiobutton
5. Click *Copy*
