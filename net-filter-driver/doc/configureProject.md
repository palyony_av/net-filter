To successfully configure project you need to check the following items:
1. Check Spectre is turned off:
	1. Go to *Project -> Properties -> C/C++ -> Code Generation*
	2. Check that *Spectre Mitigation* is on *Disabled* state
2. Add include directory for *fwpmu.h*. This file Visual Studio can't find by default.
	1. Go to *Project -> Properties -> C/C++ -> General*
	2. Add to the *Additional Include Directories* field a path to include files. For me (on Windows 10) is *C:\Program Files (x86)\Windows Kits\10\Include\10.0.18362.0\um\ *
3. Add include directory for *wdf.h*. This file Visual Studio can't find by default.
	1. Go to *Project -> Properties -> C/C++ -> General*
	2. Add to the *Additional Include Directories* field a path to include files. For me (on Windows 10) is *C:\Program Files (x86)\Windows Kits\10\Include\wdf\kmdf\1.27\ *
