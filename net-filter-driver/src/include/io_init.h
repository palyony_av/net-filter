#ifndef __IO_INIT_H__
#define __IO_INIT_H__

NTSTATUS IoInit(IN PDRIVER_OBJECT pDriverObject, OUT PDEVICE_OBJECT* ppIoDeviceObject,
	IN PDRIVER_CTX pCtx);

VOID IoUninit(PDEVICE_OBJECT pIoDeviceObject);

#endif // !__IO_INIT_H__
