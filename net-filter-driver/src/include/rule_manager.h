#ifndef __RULE_MANAGER_H__
#define __RULE_MANAGER_H__

#include "../util/include/ctx.h"

#pragma pack(push, 1)
typedef struct
{
	UINT8 ruleId;
	UINT8 direction;
} DEL_RULE_INFO, *PDEL_RULE_INFO;
#pragma pack(pop)

VOID InitRuleManager(IN PDRIVER_WFP_CTX pDriverWfpCtx);
VOID UninitRuleManager(OUT PDRIVER_WFP_CTX pDriverWfpCtx);
NTSTATUS addRule(const PFILTER_CFG pCfg);
NTSTATUS delRule(const PDEL_RULE_INFO pDelRuleInfo);
VOID clearRules();

#endif // !__RULE_MANAGER_H__
