#ifndef __IO_DEVICE_CALLBACK_H__
#define __IO_DEVICE_CALLBACK_H__

VOID InitIoDeviceCallbackGlobals(PDRIVER_CTX pDriverCtx);

_Dispatch_type_(IRP_MJ_CREATE) DRIVER_DISPATCH DispatchCreateDevice;
_Dispatch_type_(IRP_MJ_CLEANUP) DRIVER_DISPATCH DispatchCleanupDevice;
_Dispatch_type_(IRP_MJ_CLOSE) DRIVER_DISPATCH DispatchCloseDevice;
_Dispatch_type_(IRP_MJ_READ) DRIVER_DISPATCH DispatchReadAVData;
_Dispatch_type_(IRP_MJ_DEVICE_CONTROL) DRIVER_DISPATCH DispatchControlDevice;

#endif // !__IO_DEVICE_CALLBACK_H__
