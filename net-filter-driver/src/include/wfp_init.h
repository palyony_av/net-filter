#ifndef __INIT_WFP_H__
#define __INIT_WFP_H__

NTSTATUS WfpInit(_Inout_ PDRIVER_CTX pDriverCtx);
VOID WfpUninit(PDRIVER_WFP_CTX pDriverWfpCtx);

NTSTATUS WfpRegisterNetworkFilter(_Inout_ PDRIVER_WFP_CTX pDriverWfpCtx);

VOID WfpUnregisterNetworkFilter(_Inout_ PDRIVER_WFP_CTX pDriverWfpCtx);

#endif // !__INIT_WFP_H__
