#ifndef __WFP_CALLOUT_CALLBACK_H__
#define __WFP_CALLOUT_CALLBACK_H__

#include <ntddk.h>
#include <fwpsk.h>

#include "../src/util/include/ctx.h"

VOID NTAPI NetFilterFilterInboundCallback(_In_ const FWPS_INCOMING_VALUES* inFixedValues,
	_In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues, _Inout_opt_ void* layerData,
	_In_opt_ const void* classifyContext, _In_ const FWPS_FILTER* filter,
	_In_ UINT64 flowContext, _Inout_ FWPS_CLASSIFY_OUT* classifyOut);

VOID NTAPI NetFilterFilterOutboundCallback(_In_ const FWPS_INCOMING_VALUES* inFixedValues,
	_In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues, _Inout_opt_ void* layerData,
	_In_opt_ const void* classifyContext, _In_ const FWPS_FILTER* filter,
	_In_ UINT64 flowContext, _Inout_ FWPS_CLASSIFY_OUT* classifyOut);

NTSTATUS NTAPI NetFilterNotifyCallback(_In_ FWPS_CALLOUT_NOTIFY_TYPE notifyType,
	_In_ const GUID* filterKey, _Inout_ FWPS_FILTER* filter);

VOID NTAPI NetFilterFlowDeleteCallback(_In_ UINT16 layerId, _In_ UINT32 calloutId, _In_ UINT64 flowContext);

VOID NetFilterInitCallbackGlobals(PDRIVER_WFP_CTX pDriverWfpCtx, PTRANSACTION_STRUCT pTransaction);

#endif // !__WFP_CALLOUT_CALLBACK_H__
