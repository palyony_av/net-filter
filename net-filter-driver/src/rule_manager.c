#include "include/rule_manager.h"

#define CONFIG_POOL_TAG '1gfc'

static PDRIVER_WFP_CTX g_pDriverWfpCtx = NULL;

static NTSTATUS insertConfigRule(PFILTER_CFG* ppExistingCfg, PUINT8 pExistingCfgNum, const PFILTER_CFG pInCfg);
static NTSTATUS removeConfigRule(const PDEL_RULE_INFO pDelRuleInfo, PFILTER_CFG* ppExistingCfg, PUINT8 pExistingCfgNum);

VOID InitRuleManager(IN PDRIVER_WFP_CTX pDriverWfpCtx)
{
	g_pDriverWfpCtx = pDriverWfpCtx;
}

VOID UninitRuleManager(OUT PDRIVER_WFP_CTX pDriverWfpCtx)
{
	{
		for (UINT8 i = 0; i < pDriverWfpCtx->inboundCounter; ++i)
		{
			PFILTER_CFG cfg = &pDriverWfpCtx->pInboundFilterCfg[i];
			if (cfg->patternData != NULL)
			{
				ExFreePoolWithTag(cfg->patternData, CONFIG_POOL_TAG);
				cfg->patternData = NULL;
			}
		}
	}

	{
		for (UINT8 i = 0; i < pDriverWfpCtx->outboundCounter; ++i)
		{
			PFILTER_CFG cfg = &pDriverWfpCtx->pOutboundFilterCfg[i];
			if (cfg->patternData != NULL)
			{
				ExFreePoolWithTag(cfg->patternData, CONFIG_POOL_TAG);
				cfg->patternData = NULL;
			}
		}
	}

	if (pDriverWfpCtx->pInboundFilterCfg != NULL)
	{
		ExFreePoolWithTag(pDriverWfpCtx->pInboundFilterCfg, CONFIG_POOL_TAG);
		pDriverWfpCtx->pInboundFilterCfg = NULL;
	}

	if (pDriverWfpCtx->pOutboundFilterCfg != NULL)
	{
		ExFreePoolWithTag(pDriverWfpCtx->pOutboundFilterCfg, CONFIG_POOL_TAG);
		pDriverWfpCtx->pOutboundFilterCfg = NULL;
	}

	pDriverWfpCtx->inboundCounter = 0;
	pDriverWfpCtx->outboundCounter = 0;
}

NTSTATUS addRule(const PFILTER_CFG pCfg)
{
	PDRIVER_WFP_CTX pDriverWfpCtx = g_pDriverWfpCtx;

	if (pCfg->parameters.bit.direction == INBOUND_DIRECTION)
	{
#pragma warning(suppress: 4366) // The result of the unary '&' operator may be unaligned
		return insertConfigRule(&(pDriverWfpCtx->pInboundFilterCfg),
			&(pDriverWfpCtx->inboundCounter), pCfg);
	} else {
#pragma warning(suppress: 4366) // The result of the unary '&' operator may be unaligned
		return insertConfigRule(&pDriverWfpCtx->pOutboundFilterCfg,
			&pDriverWfpCtx->outboundCounter, pCfg);
	}
}

NTSTATUS delRule(const PDEL_RULE_INFO pDelRuleInfo)
{
	PDRIVER_WFP_CTX pDriverWfpCtx = g_pDriverWfpCtx;

	if (pDelRuleInfo->direction == INBOUND_DIRECTION)
	{
#pragma warning(suppress: 4366) // The result of the unary '&' operator may be unaligned
		return removeConfigRule(pDelRuleInfo, &pDriverWfpCtx->pInboundFilterCfg,
			&pDriverWfpCtx->inboundCounter);
	} else {
#pragma warning(suppress: 4366) // The result of the unary '&' operator may be unaligned
		return removeConfigRule(pDelRuleInfo, &pDriverWfpCtx->pOutboundFilterCfg,
			&pDriverWfpCtx->outboundCounter);
	}
}

VOID clearRules()
{
	UninitRuleManager(g_pDriverWfpCtx);
}

NTSTATUS insertConfigRule(PFILTER_CFG * ppExistingCfg, PUINT8 pExistingCfgNum, const PFILTER_CFG pInCfg)
{
	UINT8 filterCfgCounter = *pExistingCfgNum;
	PFILTER_CFG pFilterCfg = *ppExistingCfg;

	if (filterCfgCounter == MAX_FLT_CONFIGS)
	{
		return STATUS_UNSUCCESSFUL;
	}

	/* Check id integrity */
	for (UINT8 i = 0; i < filterCfgCounter; ++i)
	{
		if (pFilterCfg[i].ruleId == pInCfg->ruleId)
		{
			return STATUS_UNSUCCESSFUL;
		}
	}

	if (filterCfgCounter > 0)
	{
		PFILTER_CFG pNewCfg = NULL;
		UINT8 newCfgCounter = filterCfgCounter + 1;
		PUINT8 pBytePattern = NULL;
		PFILTER_CFG pNewCfgArr = ExAllocatePoolWithTag(NonPagedPool, newCfgCounter * sizeof(FILTER_CFG), CONFIG_POOL_TAG);
		if (pNewCfgArr == NULL)
		{
			return STATUS_UNSUCCESSFUL;
		}

		pBytePattern = ExAllocatePoolWithTag(NonPagedPool, pInCfg->pattern.param.length, CONFIG_POOL_TAG);
		if (pBytePattern == NULL)
		{
			ExFreePoolWithTag(pNewCfgArr, CONFIG_POOL_TAG);
			return STATUS_UNSUCCESSFUL;
		}

		RtlCopyMemory(pNewCfgArr, pFilterCfg, sizeof(FILTER_CFG) * filterCfgCounter);
		pNewCfg = &pNewCfgArr[filterCfgCounter];
		RtlCopyMemory(pNewCfg, pInCfg, sizeof(FILTER_CFG));
		RtlCopyMemory(pBytePattern, &pInCfg[1], pInCfg->pattern.param.length);

		ExFreePoolWithTag(pFilterCfg, CONFIG_POOL_TAG);
		pNewCfg->patternData = pBytePattern;
		pFilterCfg = pNewCfgArr;
		filterCfgCounter = newCfgCounter;
	} else {
		PUINT8 pBytePattern = NULL;
		PFILTER_CFG pNewCfg = ExAllocatePoolWithTag(NonPagedPool, sizeof(FILTER_CFG), CONFIG_POOL_TAG);
		if (pNewCfg == NULL)
		{
			return STATUS_UNSUCCESSFUL;
		}

		pBytePattern = ExAllocatePoolWithTag(NonPagedPool, pInCfg->pattern.param.length, CONFIG_POOL_TAG);
		if (pBytePattern == NULL)
		{
			ExFreePoolWithTag(pNewCfg, CONFIG_POOL_TAG);
			return STATUS_UNSUCCESSFUL;
		}

		RtlCopyMemory(pNewCfg, pInCfg, sizeof(FILTER_CFG));
		RtlCopyMemory(pBytePattern, &pInCfg[1], pInCfg->pattern.param.length);

		pNewCfg->patternData = pBytePattern;
		pFilterCfg = pNewCfg;
		filterCfgCounter = 1;
	}

	*ppExistingCfg = pFilterCfg;
	*pExistingCfgNum = filterCfgCounter;

	return STATUS_SUCCESS;
}

NTSTATUS removeConfigRule(const PDEL_RULE_INFO pDelRuleInfo, PFILTER_CFG* ppExistingCfg, PUINT8 pExistingCfgNum)
{
	UINT8 filterCfgCounter = *pExistingCfgNum;
	PFILTER_CFG pFilterCfg = *ppExistingCfg;

	UINT16 ruleNum = 0xFFFF;

	for (UINT8 i = 0; i < filterCfgCounter; ++i)
	{
		if (pFilterCfg[i].ruleId == pDelRuleInfo->ruleId)
		{
			ruleNum = i;
			break;
		}
	}

	if (ruleNum == 0xFFFF)
	{
		return STATUS_UNSUCCESSFUL;
	}

	if (filterCfgCounter > 1)
	{
		if ((ruleNum + 1) < filterCfgCounter)
		{
			UINT8 countRulesToMove = (UINT8)(filterCfgCounter - (ruleNum + 1));
			RtlMoveMemory(&pFilterCfg[ruleNum], &pFilterCfg[ruleNum + 1], sizeof(FILTER_CFG) * countRulesToMove);
		}
		filterCfgCounter -= 1;
	} else {
		ExFreePoolWithTag(pFilterCfg, CONFIG_POOL_TAG);
		pFilterCfg = NULL;
		filterCfgCounter = 0;
	}

	*ppExistingCfg = pFilterCfg;
	*pExistingCfgNum = filterCfgCounter;

	return STATUS_SUCCESS;
}
