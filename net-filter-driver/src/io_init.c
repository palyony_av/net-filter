#include <ntddk.h>
#include <wdmsec.h>

#define INITGUID
#include <guiddef.h>

#include "include/debug.h"
#include "../src/util/include/ctx.h"
#include "../src/include/io_device_callback.h"

#include "../src/include/io_init.h"

#define IO_DEVICE_NAME RTL_CONSTANT_STRING(L"\\Device\\PalyonyAVNetFilter")
#define IO_SYMLINK_NAME RTL_CONSTANT_STRING(L"\\??\\PalyonyAVLinkNetFilter")

// {D3FEF8A9-1BCF-4EA7-A9E7-D4D2DCC1E232}
DEFINE_GUID(IO_DEVICE_GUID,
	0xd3fef8a9, 0x1bcf, 0x4ea7, 0xa9, 0xe7, 0xd4, 0xd2, 0xdc, 0xc1, 0xe2, 0x32);

static NTSTATUS CommunicationDeviceCreate(PDRIVER_OBJECT pDriverObj, PDEVICE_OBJECT* pIoDeviceObj);
static VOID CommunicationDeviceDelete(PDEVICE_OBJECT pIoDeviceObj);
static NTSTATUS CommunicationSymlinkCreate();
static VOID CommunicationSymlinkDelete();

NTSTATUS IoInit(IN PDRIVER_OBJECT pDriverObject, OUT PDEVICE_OBJECT* ppIoDeviceObject,
	IN PDRIVER_CTX pDriverCtx)
{
	NTSTATUS status = STATUS_SUCCESS;

	status = CommunicationDeviceCreate(pDriverObject, ppIoDeviceObject);
	if (!NT_SUCCESS(status))
	{
		KdPrint(("%s%s: CommunicationDeviceCreate fail\r\n", DEBUG_CONST, __FUNCTION__));
		goto Exit;
	}

	status = CommunicationSymlinkCreate();
	if (!NT_SUCCESS(status))
	{
		KdPrint(("%s%s: CommunicationSymlinkCreate fail\r\n", DEBUG_CONST, __FUNCTION__));
		CommunicationDeviceDelete(*ppIoDeviceObject);
		goto Exit;
	}

	InitIoDeviceCallbackGlobals(pDriverCtx);

	pDriverObject->MajorFunction[IRP_MJ_CREATE] = DispatchCreateDevice;
	pDriverObject->MajorFunction[IRP_MJ_CLOSE] = DispatchCloseDevice;
	pDriverObject->MajorFunction[IRP_MJ_READ] = DispatchReadAVData;
	pDriverObject->MajorFunction[IRP_MJ_CLEANUP] = DispatchCleanupDevice;
	pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchControlDevice;

Exit:
	return status;
}

VOID IoUninit(PDEVICE_OBJECT pIoDeviceObject)
{
	if (pIoDeviceObject == NULL)
	{
		return;
	}

	CommunicationSymlinkDelete();
	CommunicationDeviceDelete(pIoDeviceObject);
}

NTSTATUS CommunicationDeviceCreate(PDRIVER_OBJECT pDriverObj, PDEVICE_OBJECT* pIoDeviceObj)
{
	UNICODE_STRING deviceName = IO_DEVICE_NAME;
	return IoCreateDeviceSecure(pDriverObj, 0, &deviceName, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &SDDL_DEVOBJ_SYS_ALL_ADM_ALL, &IO_DEVICE_GUID, pIoDeviceObj);
}

VOID CommunicationDeviceDelete(PDEVICE_OBJECT pIoDeviceObj)
{
	IoDeleteDevice(pIoDeviceObj);
}

NTSTATUS CommunicationSymlinkCreate()
{
	UNICODE_STRING symbolicLinkName = IO_SYMLINK_NAME;
	UNICODE_STRING deviceName = IO_DEVICE_NAME;

	return IoCreateSymbolicLink(&symbolicLinkName, &deviceName);
}

VOID CommunicationSymlinkDelete()
{
	UNICODE_STRING symbolicLinkName = IO_SYMLINK_NAME;

	IoDeleteSymbolicLink(&symbolicLinkName);
}
