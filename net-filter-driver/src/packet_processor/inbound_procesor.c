#include "../include/debug.h"
#include "include/ipv4_protocol.h"
#include "include/udp_protocol.h"
#include "include/tcp_protocol.h"
#include "include/icmp_protocol.h"
#include "include/common.h"

#include "include/inbound_procesor.h"

static NTSTATUS getIpv4Header(PVOID layerData, PIPV4_HEADER* pIpv4Header);
static FWP_ACTION_TYPE processRule(PFILTER_CFG cfg, PIPV4_HEADER pIpv4Header,
	PTRANSACTION_STRUCT pTransaction);

FWP_ACTION_TYPE processInboundIpv4Rules(PFILTER_CFG cfg, UINT8 numCfg, PVOID layerData, 
	PTRANSACTION_STRUCT pTransaction)
{
	FWP_ACTION_TYPE decision = FWP_ACTION_PERMIT;

	PIPV4_HEADER pIpv4Header;

	/* If we are could not access packet then permit packet by default */
	if (!NT_SUCCESS(getIpv4Header(layerData, &pIpv4Header)))
	{
		decision = FWP_ACTION_PERMIT;
		goto Exit;
	}

	for (UINT8 i = 0; i < numCfg && decision == FWP_ACTION_PERMIT; ++i)
	{
		PFILTER_CFG curCfg = &cfg[i];
		decision = processRule(curCfg, pIpv4Header, pTransaction);
	}

Exit:
	return decision;
}

NTSTATUS getIpv4Header(PVOID layerData, PIPV4_HEADER* ppIpv4Header)
{
	PNET_BUFFER_LIST pNetList = (PNET_BUFFER_LIST)layerData;
	PUINT8 wholePacketBuffer = NULL;
	ULONG packetBufferSize = 0;
	ULONG ipPacketOffset = 0;
	ULONG ipPacketLength = 0;

	if (pNetList == NULL)
	{
		goto Exit;
	}

	wholePacketBuffer = MmGetMdlVirtualAddress(pNetList->FirstNetBuffer->CurrentMdl);
	packetBufferSize = MmGetMdlByteCount(pNetList->FirstNetBuffer->CurrentMdl);

	ipPacketOffset = pNetList->FirstNetBuffer->CurrentMdlOffset;
	ipPacketLength = pNetList->FirstNetBuffer->DataLength;
	if (packetBufferSize < ipPacketLength || ipPacketOffset < sizeof(IPV4_HEADER))
	{
		goto Exit;
	}

	/* Because of the differnet packet processing techique */
	*ppIpv4Header = (PIPV4_HEADER)(wholePacketBuffer + ipPacketOffset - sizeof(IPV4_HEADER));

	return STATUS_SUCCESS;
Exit:
	return STATUS_UNSUCCESSFUL;
}

FWP_ACTION_TYPE processRule(PFILTER_CFG cfg, PIPV4_HEADER pIpv4Header,
	PTRANSACTION_STRUCT pTransaction)
{
	FWP_ACTION_TYPE decision = FWP_ACTION_PERMIT;
	PIN_ADDR pLocalIp = &pIpv4Header->dst;
	PIN_ADDR pRemoteIp = &pIpv4Header->src;

	/* If in rule IP_PROTOCOL is set so we are match all packets */
	if (cfg->parameters.bit.tProtocol != IP_PROTOCOL
		&& cfg->parameters.bit.tProtocol != pIpv4Header->proto)
	{
		decision = FWP_ACTION_PERMIT;
		goto Exit;
	}

	if ((cfg->remote.addr.S_un.S_addr & cfg->remote.mask) == (pRemoteIp->S_un.S_addr & cfg->remote.mask) &&
		(cfg->local.addr.S_un.S_addr & cfg->local.mask) == (pLocalIp->S_un.S_addr & cfg->local.mask))
	{
		UINT16 ipv4HeaderLength = 4 * (pIpv4Header->ihlAndVersion & 0x0F);

		switch (cfg->parameters.bit.tProtocol)
		{
		case TCP_PROTOCOL:
		{
#pragma warning(suppress: 6305)
			PTCP_HEADER pTcpHeader = (PTCP_HEADER)((PUINT8)pIpv4Header + ipv4HeaderLength);
			
			/* Swap bytes because the protocol spec in big endian, but packet store in memory in little endian */
			UINT16 remotePort = RtlUshortByteSwap(pTcpHeader->srcPort);
			UINT16 localPort = RtlUshortByteSwap(pTcpHeader->dstPort);

			if (RtlUshortByteSwap(pIpv4Header->length) <= (ipv4HeaderLength + sizeof(TCP_HEADER)))
			{
				/* Unknown packet so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			if (!(cfg->remote.port.range.start <= remotePort && remotePort <= cfg->remote.port.range.end)
				|| !(cfg->local.port.range.start <= localPort && localPort <= cfg->local.port.range.end))
			{
				/* The packet is under rule so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			UINT16 payloadTcpOffset = 4 * ((pTcpHeader->offsetAndFlags & 0x00F0) >> 4);
			PUINT8 payload = (PUINT8)pTcpHeader + payloadTcpOffset;
			// TODO: Need to make more readable
			UINT16 payloadLength = RtlUshortByteSwap(pIpv4Header->length) - (ipv4HeaderLength + payloadTcpOffset);

			if (cfg->parameters.bit.useBytePattern == 1 && !NT_SUCCESS(checkMemoryPattern(cfg, payload, payloadLength)))
			{
				/* The payload in packet is not match so permit packet */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}
		} break;
		case UDP_PROTOCOL:
		{
			PUDP_HEADER pUdpHeader = (PUDP_HEADER)((PUINT8)pIpv4Header + ipv4HeaderLength);

			/* Swap bytes because the protocol spec in big endian, but packet store in memory in little endian */
			UINT16 remotePort = RtlUshortByteSwap(pUdpHeader->srcPort);
			UINT16 localPort = RtlUshortByteSwap(pUdpHeader->dstPort);

			if (RtlUshortByteSwap(pIpv4Header->length) != (
				ipv4HeaderLength + RtlUshortByteSwap(pUdpHeader->length)
			))
			{
				/* Unknown packet so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			if (!(cfg->remote.port.range.start <= remotePort && remotePort <= cfg->remote.port.range.end)
				|| !(cfg->local.port.range.start <= localPort && localPort <= cfg->local.port.range.end))
			{
				/* The packet is under rule so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			UINT16 payloadUdpOffset = sizeof(UDP_HEADER);
			PUINT8 payload = (PUINT8)pUdpHeader + payloadUdpOffset;
			UINT16 payloadLength = RtlUshortByteSwap(pUdpHeader->length);

			if (cfg->parameters.bit.useBytePattern == 1 && !NT_SUCCESS(checkMemoryPattern(cfg, payload, payloadLength)))
			{
				/* The payload in packet is not match so permit packet */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}
		} break;
		case ICMP_PROTOCOL:
		{
			/* Do nothing beacuse the packet does not have fields to filter */
		} break;
		case IP_PROTOCOL:
		{
			/* Do nothing beacuse the packet does not have fields to filter */
		} break;
		default:
		{
			goto Exit;
		} break;
		}

		switch (cfg->action)
		{
		case IPV4_BLOCK_TRAFFIC:
			decision = FWP_ACTION_BLOCK;
			break;
		case IPV4_LOG_TRAFFIC:
		{
			switch (cfg->parameters.bit.tProtocol)
			{
			case TCP_PROTOCOL:
			{
				KdPrint(("%s[%u][tcp] IN %u.%u.%u.%u -> %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][tcp] IN %u.%u.%u.%u -> %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case UDP_PROTOCOL:
			{
				KdPrint(("%s[%u][udp] IN %u.%u.%u.%u -> %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][udp] IN %u.%u.%u.%u -> %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case ICMP_PROTOCOL:
			{
				KdPrint(("%s[%u][icmp] IN %u.%u.%u.%u -> %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][icmp] IN %u.%u.%u.%u -> %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case IP_PROTOCOL:
			{
				KdPrint(("%s[%u][ip] IN %u.%u.%u.%u -> %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][ip] IN %u.%u.%u.%u -> %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			}
			decision = FWP_ACTION_PERMIT;
		} break;
		default:
		{
			decision = FWP_ACTION_PERMIT;
		}break;
		}
	}

Exit:
	return decision;
}
