#include "../include/debug.h"
#include "include/ipv4_protocol.h"
#include "include/tcp_protocol.h"
#include "include/udp_protocol.h"
#include "include/icmp_protocol.h"
#include "include/common.h"

#include "../../src/packet_processor/include/outbound_processor.h"

typedef struct
{
	PIPV4_HEADER pHeader;
	PUINT8 payload;			/* payload data*/
	UINT16 payloadSize;		/* payload size */
} OUT_IPV4_PROTOCOL, * POUT_IPV4_PROTOCOL;

static NTSTATUS getOutboundIpv4Header(PVOID layerData, POUT_IPV4_PROTOCOL pOutboundIpv4Protocol);
static FWP_ACTION_TYPE processRule(PFILTER_CFG cfg, POUT_IPV4_PROTOCOL pOutboundIpv4Protocol,
	PTRANSACTION_STRUCT pTransaction);

FWP_ACTION_TYPE processOutboundIpRules(PFILTER_CFG cfg, UINT8 numCfg, PVOID layerData,
	PTRANSACTION_STRUCT pTransaction)
{
	FWP_ACTION_TYPE decision = FWP_ACTION_PERMIT;
	OUT_IPV4_PROTOCOL ipv4Protocol = { 0 };

	if (!NT_SUCCESS(getOutboundIpv4Header(layerData, &ipv4Protocol)))
	{
		decision = FWP_ACTION_PERMIT;
		goto Exit;
	}

	for (UINT8 i = 0; i < numCfg && decision == FWP_ACTION_PERMIT; ++i)
	{
		PFILTER_CFG curCfg = &cfg[i];
		decision = processRule(curCfg, &ipv4Protocol, pTransaction);
	}

Exit:
	return decision;
}

NTSTATUS getOutboundIpv4Header(PVOID layerData, POUT_IPV4_PROTOCOL pOutboundIpv4Protocol)
{
	PNET_BUFFER_LIST pNetList = (PNET_BUFFER_LIST)layerData;
	PUINT8 wholePacketBuffer = NULL;
	PIPV4_HEADER pIpv4Header = NULL;
	ULONG packetBufferSize = 0;
	ULONG ipPacketOffset = 0;
	ULONG ipPacketLength = 0;

	if (pNetList == NULL)
	{
		goto Exit;
	}

	wholePacketBuffer = MmGetMdlVirtualAddress(pNetList->FirstNetBuffer->CurrentMdl);
	packetBufferSize = MmGetMdlByteCount(pNetList->FirstNetBuffer->CurrentMdl);

	ipPacketOffset = pNetList->FirstNetBuffer->CurrentMdlOffset;
	ipPacketLength = pNetList->FirstNetBuffer->DataLength;

	if (packetBufferSize < ipPacketLength || ipPacketLength < sizeof(IPV4_HEADER))
	{
		goto Exit;
	}

	pIpv4Header = (PIPV4_HEADER)(wholePacketBuffer + ipPacketOffset);

	pOutboundIpv4Protocol->pHeader = pIpv4Header;

	if (pNetList->FirstNetBuffer->CurrentMdl->Next != NULL)
	{
		pOutboundIpv4Protocol->payload = (PUINT8)MmGetMdlVirtualAddress(pNetList->FirstNetBuffer->CurrentMdl->Next);
		pOutboundIpv4Protocol->payloadSize = (UINT16)MmGetMdlByteCount(pNetList->FirstNetBuffer->CurrentMdl->Next);
	}

	return STATUS_SUCCESS;

Exit:
	return STATUS_UNSUCCESSFUL;
}

FWP_ACTION_TYPE processRule(PFILTER_CFG cfg, POUT_IPV4_PROTOCOL pOutboundIpv4Protocol,
	PTRANSACTION_STRUCT pTransaction)
{
	FWP_ACTION_TYPE decision = FWP_ACTION_PERMIT;
	PIN_ADDR pLocalIp = &pOutboundIpv4Protocol->pHeader->src;
	PIN_ADDR pRemoteIp = &pOutboundIpv4Protocol->pHeader->dst;

	/* If in rule IP_PROTOCOL is set so we are match all packets */
	if (cfg->parameters.bit.tProtocol != IP_PROTOCOL
		&& cfg->parameters.bit.tProtocol != pOutboundIpv4Protocol->pHeader->proto)
	{
		decision = FWP_ACTION_PERMIT;
		goto Exit;
	}

	if ((cfg->remote.addr.S_un.S_addr & cfg->remote.mask) == (pRemoteIp->S_un.S_addr & cfg->remote.mask) &&
		(cfg->local.addr.S_un.S_addr & cfg->local.mask) == (pLocalIp->S_un.S_addr & cfg->local.mask))
	{
		UINT16 ipv4HeaderLength = 4 * (pOutboundIpv4Protocol->pHeader->ihlAndVersion & 0x0F);

		switch (cfg->parameters.bit.tProtocol)
		{
		case TCP_PROTOCOL:
		{
#pragma warning(suppress: 6305)
			PTCP_HEADER pTcpHeader = (PTCP_HEADER)((PUINT8)pOutboundIpv4Protocol->pHeader + ipv4HeaderLength);
			
			/* Swap bytes because the protocol spec in big endian, but packet store in memory in little endian */
			UINT16 remotePort = RtlUshortByteSwap(pTcpHeader->dstPort);
			UINT16 localPort = RtlUshortByteSwap(pTcpHeader->srcPort);

			if (RtlUshortByteSwap(pOutboundIpv4Protocol->pHeader->length) <= (ipv4HeaderLength + sizeof(TCP_HEADER)))
			{
				/* Unknown packet so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			if (!(cfg->remote.port.range.start <= remotePort && remotePort <= cfg->remote.port.range.end)
				|| !(cfg->local.port.range.start <= localPort && localPort <= cfg->local.port.range.end))
			{
				goto Exit;
			}

			if (cfg->parameters.bit.useBytePattern == 1 
			&& !NT_SUCCESS(checkMemoryPattern(cfg, pOutboundIpv4Protocol->payload, pOutboundIpv4Protocol->payloadSize)))
			{
				goto Exit;
			}
		} break;
		case UDP_PROTOCOL:
		{
			PUDP_HEADER pUdpHeader = (PUDP_HEADER)((PUINT8)pOutboundIpv4Protocol->pHeader + ipv4HeaderLength);

			/* Swap bytes because the protocol spec in big endian, but packet store in memory in little endian */
			UINT16 remotePort = RtlUshortByteSwap(pUdpHeader->srcPort);
			UINT16 localPort = RtlUshortByteSwap(pUdpHeader->dstPort);

			if (RtlUshortByteSwap(pOutboundIpv4Protocol->pHeader->length) != (
				ipv4HeaderLength + RtlUshortByteSwap(pUdpHeader->length)
				))
			{
				/* Unknown packet so permit by default */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			if (RtlUshortByteSwap(pUdpHeader->length) != (pOutboundIpv4Protocol->payloadSize + sizeof(UDP_HEADER)))
			{
				/* The size in header is not the same as the size in payload chunk */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}

			if (!(cfg->remote.port.range.start <= remotePort && remotePort <= cfg->remote.port.range.end)
				|| !(cfg->local.port.range.start <= localPort && localPort <= cfg->local.port.range.end))
			{
				goto Exit;
			}

			PUINT8 payload = pOutboundIpv4Protocol->payload;
			UINT16 payloadLength = pOutboundIpv4Protocol->payloadSize;

			if (cfg->parameters.bit.useBytePattern == 1 && !NT_SUCCESS(checkMemoryPattern(cfg, payload, payloadLength)))
			{
				/* The payload in packet is not match so permit packet */
				decision = FWP_ACTION_PERMIT;
				goto Exit;
			}
		} break;
		case ICMP_PROTOCOL:
		{
			/* Do nothing beacuse the packet does not have fields to filter */
		} break;
		case IP_PROTOCOL:
		{
			/* Do nothing beacuse the packet does not have fields to filter */
		} break;
		default:
		{
			goto Exit;
		} break;
		}

		switch (cfg->action)
		{
		case IPV4_BLOCK_TRAFFIC:
			decision = FWP_ACTION_BLOCK;
			break;
		case IPV4_LOG_TRAFFIC:
		{
			switch (cfg->parameters.bit.tProtocol)
			{
			case TCP_PROTOCOL:
			{
				KdPrint(("%s[%u][tcp] OUT %u.%u.%u.%u <- %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][tcp] OUT %u.%u.%u.%u <- %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case UDP_PROTOCOL:
			{
				KdPrint(("%s[%u][udp] OUT %u.%u.%u.%u <- %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][udp] OUT %u.%u.%u.%u <- %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case ICMP_PROTOCOL:
			{
				KdPrint(("%s[%u][icmp] OUT %u.%u.%u.%u <- %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][icmp] OUT %u.%u.%u.%u <- %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			case IP_PROTOCOL:
			{
				KdPrint(("%s[%u][ip] OUT %u.%u.%u.%u <- %u.%u.%u.%u\n", DEBUG_CONST, cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4));
				TransactionBufferPushA(pTransaction, "[%u][ip] OUT %u.%u.%u.%u <- %u.%u.%u.%u", cfg->ruleId,
					pRemoteIp->S_un.S_un_b.s_b1, pRemoteIp->S_un.S_un_b.s_b2, pRemoteIp->S_un.S_un_b.s_b3, pRemoteIp->S_un.S_un_b.s_b4,
					pLocalIp->S_un.S_un_b.s_b1, pLocalIp->S_un.S_un_b.s_b2, pLocalIp->S_un.S_un_b.s_b3, pLocalIp->S_un.S_un_b.s_b4);
			} break;
			}
			decision = FWP_ACTION_PERMIT;
		} break;
		default:
		{
			decision = FWP_ACTION_PERMIT;
		}break;
		}
	}

Exit:
	return decision;
}
