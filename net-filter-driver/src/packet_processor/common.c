#include "include/common.h"

NTSTATUS checkMemoryPattern(PFILTER_CFG cfg, PUINT8 pMemory, UINT16 memoryLength)
{
	PUINT8 targetPlace = (PUINT8)(pMemory + cfg->pattern.param.offset);
	NTSTATUS status = STATUS_UNSUCCESSFUL;

	if ((cfg->pattern.param.offset + cfg->pattern.param.length) > memoryLength)
	{
		status = STATUS_UNSUCCESSFUL;
		goto Exit;
	}

	/*KdPrint(("payload %p | target %p", pMemory, targetPlace));

	KdPrint(("cfg %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX",
		cfg->patternData[0], cfg->patternData[1], cfg->patternData[2], cfg->patternData[3],
		cfg->patternData[4], cfg->patternData[5], cfg->patternData[6], cfg->patternData[7]));
	KdPrint(("packet %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX",
		targetPlace[0], targetPlace[1], targetPlace[2], targetPlace[3],
		targetPlace[4], targetPlace[5], targetPlace[6], targetPlace[7]));*/

	if (RtlCompareMemory(targetPlace, cfg->patternData, cfg->pattern.param.length) == cfg->pattern.param.length)
	{
		status = STATUS_SUCCESS;
	}

	//KdPrint(("Memory compared %u with length %u", status, cfg->pattern.param.length));

Exit:
	return status;
}
