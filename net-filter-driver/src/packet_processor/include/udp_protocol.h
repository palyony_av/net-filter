#ifndef __UDP_PROTOCOL_H__
#define __UDP_PROTOCOL_H__

#include <ntddk.h>

#pragma pack(push)

typedef struct {
	UINT16 srcPort;
	UINT16 dstPort;
	UINT16 length;			/* The length in bytes of the UDP header and UDP data */
	UINT16 checksum;
} UDP_HEADER, * PUDP_HEADER;

#pragma pack(pop)

#endif // !__UDP_PROTOCOL_H__
