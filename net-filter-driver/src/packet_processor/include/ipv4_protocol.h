#ifndef __IPV4_PROTOCOL_H__
#define __IPV4_PROTOCOL_H__

#include <ntddk.h>
#include <fwpsk.h>

#pragma pack(push)

/* IPv4 header in little endian */
typedef struct
{
	UINT8	ihlAndVersion;	/* header length and version */

	UINT8	tos;			/* type of service */
	UINT16	length;			/* total length. (FAKE) */
	UINT16	id;				/* identification */
	UINT16	offset;			/* fragment offset field */
	UINT8	ttl;			/* time to live */
	UINT8	proto;			/* protocol */
	UINT16	sum;			/* checksum. (FAKE) */
	IN_ADDR	src;			/* source address */
	IN_ADDR	dst;			/* dest address */
} IPV4_HEADER, * PIPV4_HEADER;

#pragma pack(pop)

#endif // !__IPV4_PROTOCOL_H__
