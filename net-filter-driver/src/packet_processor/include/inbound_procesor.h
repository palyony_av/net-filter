#ifndef __INBOUND_PROCESSOR_H__
#define __INBOUND_PROCESSOR_H__

#include <fwpsk.h>

#include "../../util/include/ctx.h"

FWP_ACTION_TYPE processInboundIpv4Rules(PFILTER_CFG cfg, UINT8 numCfg, PVOID layerData,
	PTRANSACTION_STRUCT pTransaction);

#endif // !__INBOUND_PROCESSOR_H__
