#ifndef __ICMP_PROTOCOL_H__
#define __ICMP_PROTOCOL_H__

#include <ntddk.h>

#pragma pack(push)

typedef struct {
	UINT8 type;
	UINT8 code;
	UINT16 checksum;
	UINT32 restOfHeader;
} ICMP_HEADER, * PICMP_HEADER;

#pragma pack(pop)

#endif // !__ICMP_PROTOCOL_H__
