#ifndef __COMMON_H__
#define __COMMON_H__

#include "../../util/include/ctx.h"

NTSTATUS checkMemoryPattern(PFILTER_CFG cfg, PUINT8 pMemory, UINT16 memoryLength);

#endif // !__COMMON_H__
