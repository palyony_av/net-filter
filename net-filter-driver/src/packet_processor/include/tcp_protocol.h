#ifndef __TCP_PROTOCOL_H__
#define __TCP_PROTOCOL_H__

#include <ntddk.h>

#pragma pack(push)

typedef struct {
	UINT16 srcPort;
	UINT16 dstPort;
	UINT32 seq;
	UINT32 ack;
	UINT16 offsetAndFlags;
	UINT16 windowSize;
	UINT16 checksum;		/* (FAKE) */
	UINT16 urgentPtr;
} TCP_HEADER, * PTCP_HEADER;

#pragma pack(pop)

#endif // !__TCP_PROTOCOL_H__
