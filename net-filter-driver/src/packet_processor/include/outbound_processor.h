#ifndef __OUTBOUND_PROCESSOR_H__
#define __OUTBOUND_PROCESSOR_H__

#include <fwpsk.h>

#include "../../util/include/ctx.h"

FWP_ACTION_TYPE processOutboundIpRules(PFILTER_CFG cfg, UINT8 numCfg, PVOID layerData,
	PTRANSACTION_STRUCT pTransaction);

#endif // !__OUTBOUND_PROCESSOR_H__
