#include <ntddk.h>					// Windows Driver Development Kit
#include <wdf.h>					// Windows Driver Framework

#include <fwpsk.h>
#include <fwpmk.h>
#define INITGUID
#include <guiddef.h>

#include "../src/util/include/ctx.h"
#include "../src/include/wfp_callout_callback.h"

#include "../src/include/wfp_init.h"

/*
 * In this file we are initialize callbacks, that defined in wfp_callout_callback module
 *
 * Steps:
 * 1. Open WFP Engine
 * 2. Register callout. Need device. There are two strategies:
 *		1. Create IO device
 *		2. Create network device (I choose that)
 * 3. Add callout to the WFP engine
 * 4. Add sublayer to inject it into network stack
 */

// {36E0E9D6-819E-43F1-B66D-D4994A47967D}
DEFINE_GUID(WfpInboundCalloutGUID,
	0x36e0e9d6, 0x819e, 0x43f1, 0xb6, 0x6d, 0xd4, 0x99, 0x4a, 0x47, 0x96, 0x7d);

// {36DD6089-871A-4F99-9806-FA1F59E02343}
DEFINE_GUID(WfpOutboundCalloutGUID,
	0x36dd6089, 0x871a, 0x4f99, 0x98, 0x6, 0xfa, 0x1f, 0x59, 0xe0, 0x23, 0x43);

// {83A16A10-A024-44FE-AA9A-3BD8C3ADAEB0}
DEFINE_GUID(WfpSublayerGUID,
	0x83a16a10, 0xa024, 0x44fe, 0xaa, 0x9a, 0x3b, 0xd8, 0xc3, 0xad, 0xae, 0xb0);

#define WFP_INBOUND_CALLOUT_NAME L"NetFilterInboundCallout"
#define WFP_OUTBOUND_CALLOUT_NAME L"NetFilterOutboundCallout"

#define WFP_SUBLAYER_NAME L"NetFilterSublayer"

// #define MAX_WEIGHT 0xFFFFui16
#define HIGH_WEIGHT 0xFFDCui16
// #define MIN_WEIGHT 0x0000ui16

#define WFP_FILTER_NAME L"NetFilterFilter00"
//#define WFP_FILTER_NUM_IDX 15  // On position 
//#define WFP_FILTER_DESCRIPTION L"NetFilterFilterDescription"

// Note: _WfpRegisterNetworkFilter does not exist because of a lot of arguments
static VOID _WfpUnregisterNetwork(
	_In_ HANDLE engineHandle,
	_In_ UINT32 regInboundCalloutId,
	_In_ UINT32 regOutboundCalloutId,
	_In_ UINT32 addInboundCalloutId,
	_In_ UINT32 addOutboundCalloutId);

static NTSTATUS _WfpOpenEngine(_Out_ PHANDLE pEngineHadle);
static VOID _UninitializeEngineWfp(_In_ HANDLE engineHandle);
static NTSTATUS _WfpRegisterCallout(PDEVICE_OBJECT pDeviceObject, UINT32* pRegCalloutId,
	const GUID* pCalloutGuid, FWPS_CALLOUT_CLASSIFY_FN2 mainCalloutCallback);
static NTSTATUS _WfpAddCallout(_In_ HANDLE engineHandle, _Out_ UINT32* pAddCalloutId,
	_In_ PWCHAR pCalloutName, _In_ const GUID* pCalloutGuid, _In_ const GUID* pApplicableLayer);
static NTSTATUS _WfpAddSublayer(_In_ HANDLE engineHandle, _In_ PWCHAR pSublayerName,
	_In_ const GUID* pSublayerGuid);
static NTSTATUS _WfpAddFIlter(_In_ HANDLE engineHandle, _Out_ UINT64* pFilterId, UINT8 direction);

NTSTATUS WfpInit(_Inout_ PDRIVER_CTX pDriverCtx)
{
	NTSTATUS status = STATUS_SUCCESS;
	HANDLE hEngine = NULL;
	PWCHAR pFilterName = NULL;
	UINT32 regInboundCalloutId = 0;
	UINT32 regOutboundCalloutId = 0;
	UINT32 addInboundCalloutId = 0;
	UINT32 addOutboundCalloutId = 0;
	PDRIVER_WFP_CTX pDriverWfpCtx = &pDriverCtx->wfp;
	PDEVICE_OBJECT pWdfDeviceObject = pDriverCtx->pWdfDeviceObject;

	status = ContextInitBuffer(WFP_FILTER_NAME, sizeof(WFP_FILTER_NAME), &pFilterName);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpOpenEngine(&hEngine);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpRegisterCallout(pWdfDeviceObject, &regInboundCalloutId, &WfpInboundCalloutGUID,
		NetFilterFilterInboundCallback);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpRegisterCallout(pWdfDeviceObject, &regOutboundCalloutId, &WfpOutboundCalloutGUID,
		NetFilterFilterOutboundCallback);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpAddCallout(hEngine, &addInboundCalloutId, WFP_INBOUND_CALLOUT_NAME,
		&WfpInboundCalloutGUID, &FWPM_LAYER_INBOUND_IPPACKET_V4);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpAddCallout(hEngine, &addOutboundCalloutId, WFP_OUTBOUND_CALLOUT_NAME,
		&WfpOutboundCalloutGUID, &FWPM_LAYER_OUTBOUND_IPPACKET_V4);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpAddSublayer(hEngine, WFP_SUBLAYER_NAME, &WfpSublayerGUID);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	pDriverWfpCtx->engineHandle = hEngine;
	pDriverWfpCtx->regInboundCalloutId = regInboundCalloutId;
	pDriverWfpCtx->regOutboundCalloutId = regOutboundCalloutId;
	pDriverWfpCtx->addInboundCalloutId = addInboundCalloutId;
	pDriverWfpCtx->addOutboundCalloutId = addOutboundCalloutId;

	NetFilterInitCallbackGlobals(pDriverWfpCtx, &pDriverCtx->transaction);

	return STATUS_SUCCESS;
Exit:
	ContextUninitBuffer(pFilterName);

	_WfpUnregisterNetwork(
		hEngine,
		regInboundCalloutId,
		regOutboundCalloutId,
		addInboundCalloutId,
		addOutboundCalloutId
	);

	_UninitializeEngineWfp(hEngine);
	return STATUS_UNSUCCESSFUL;
}

VOID WfpUninit(PDRIVER_WFP_CTX pDriverWfpCtx)
{
	if (pDriverWfpCtx->engineHandle == NULL)
	{
		return;
	}

	WfpUnregisterNetworkFilter(pDriverWfpCtx);

	_WfpUnregisterNetwork(
		pDriverWfpCtx->engineHandle, 
		pDriverWfpCtx->regInboundCalloutId,
		pDriverWfpCtx->regOutboundCalloutId,
		pDriverWfpCtx->addInboundCalloutId,
		pDriverWfpCtx->addOutboundCalloutId
	);

	_UninitializeEngineWfp(pDriverWfpCtx->engineHandle);
	pDriverWfpCtx->engineHandle = NULL;
}

NTSTATUS WfpRegisterNetworkFilter(_Inout_ PDRIVER_WFP_CTX pDriverWfpCtx)
{
	NTSTATUS status = STATUS_SUCCESS;

	status = _WfpAddFIlter(pDriverWfpCtx->engineHandle, &pDriverWfpCtx->inboundFilterId, INBOUND_DIRECTION);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	status = _WfpAddFIlter(pDriverWfpCtx->engineHandle, &pDriverWfpCtx->outboundFilterId, OUTBOUND_DIRECTION);
	if (!NT_SUCCESS(status))
	{
		goto Exit;
	}

	return STATUS_SUCCESS;
Exit:
	WfpUnregisterNetworkFilter(pDriverWfpCtx);
	return status;
}

VOID WfpUnregisterNetworkFilter(_Inout_ PDRIVER_WFP_CTX pDriverWfpCtx)
{
	if (pDriverWfpCtx->inboundFilterId != 0)
	{
		FwpmFilterDeleteById(pDriverWfpCtx->engineHandle, pDriverWfpCtx->inboundFilterId);
		pDriverWfpCtx->inboundFilterId = 0;
	}

	if (pDriverWfpCtx->outboundFilterId != 0)
	{
		FwpmFilterDeleteById(pDriverWfpCtx->engineHandle, pDriverWfpCtx->outboundFilterId);
		pDriverWfpCtx->outboundFilterId = 0;
	}
}

NTSTATUS _WfpOpenEngine(_Out_ PHANDLE pEngineHadle)
{
	return FwpmEngineOpen(NULL, RPC_C_AUTHN_WINNT, NULL, NULL, pEngineHadle);
}

VOID _UninitializeEngineWfp(_In_ HANDLE engineHandle)
{
	if (engineHandle != NULL)
	{
		FwpmEngineClose(engineHandle);
		engineHandle = NULL;
	}
}

VOID _WfpUnregisterNetwork(
	_In_ HANDLE engineHandle,
	_In_ UINT32 regInboundCalloutId,
	_In_ UINT32 regOutboundCalloutId,
	_In_ UINT32 addInboundCalloutId,
	_In_ UINT32 addOutboundCalloutId)
{
	if (engineHandle == NULL)
	{
		return;
	}

	FwpmSubLayerDeleteByKey(engineHandle, &WfpSublayerGUID);

	if (addInboundCalloutId != 0)
	{
		FwpmCalloutDeleteById(engineHandle, addInboundCalloutId);
	}

	if (addOutboundCalloutId != 0)
	{
		FwpmCalloutDeleteById(engineHandle, addOutboundCalloutId);
	}

	if (regInboundCalloutId != 0)
	{
		FwpsCalloutUnregisterById(regInboundCalloutId);
	}

	if (regOutboundCalloutId != 0)
	{
		FwpsCalloutUnregisterById(regOutboundCalloutId);
	}
}

NTSTATUS _WfpRegisterCallout(PDEVICE_OBJECT pDeviceObject, UINT32* pRegCalloutId, 
	const GUID* pCalloutGuid, FWPS_CALLOUT_CLASSIFY_FN2 mainCalloutCallback)
{
	FWPS_CALLOUT Callout = { 0 };

	Callout.calloutKey = *pCalloutGuid;
	Callout.flags = 0;

	Callout.classifyFn = mainCalloutCallback;
	Callout.notifyFn = NetFilterNotifyCallback;
	Callout.flowDeleteFn = NetFilterFlowDeleteCallback;
	return FwpsCalloutRegister(pDeviceObject, &Callout, pRegCalloutId);
}

NTSTATUS _WfpAddCallout(_In_ HANDLE engineHandle, _Out_ UINT32* pAddCalloutId,
	_In_ PWCHAR pCalloutName, _In_ const GUID* pCalloutGuid, _In_ const GUID* pApplicableLayer)
{
	FWPM_CALLOUT callout = { 0 };
	
	callout.flags = 0;
	callout.displayData.name = pCalloutName;
	callout.calloutKey = *pCalloutGuid;
	callout.applicableLayer = *pApplicableLayer;
	 
	return FwpmCalloutAdd(engineHandle, &callout, NULL, pAddCalloutId);
}

NTSTATUS _WfpAddSublayer(_In_ HANDLE engineHandle, _In_ PWCHAR pSublayerName,
	_In_ const GUID* pSublayerGuid)
{
	/*
	 * When two or more callouts are registered at the same sublayer, problems
	 * may occur when the same weight is assigned to the filters. This issue
	 * can be prevented by having callouts create their own sublayer by using
	 * FwpmSubLayerAdd0.
	*/

	FWPM_SUBLAYER subLayer = { 0 };

	subLayer.displayData.name = pSublayerName;
	subLayer.subLayerKey = *pSublayerGuid;
	subLayer.weight = HIGH_WEIGHT;

	return FwpmSubLayerAdd(engineHandle, &subLayer, NULL);
}

NTSTATUS _WfpAddFIlter(_In_ HANDLE engineHandle, _Out_ UINT64* pFilterId, UINT8 direction)
{
	FWPM_FILTER filter = { 0 };
	FWPM_FILTER_CONDITION condition[1] = { 0 };
	UINT64 filterWeight = FWPM_AUTO_WEIGHT_MAX;
	FWP_V4_ADDR_AND_MASK AddrRangeMask = { 0 };
	
	condition[0].matchType = FWP_MATCH_EQUAL;
	condition[0].conditionValue.type = FWP_V4_ADDR_MASK;
	condition[0].conditionValue.v4AddrMask = &AddrRangeMask;

	if (direction == INBOUND_DIRECTION)
	{
		condition[0].fieldKey = FWPM_CONDITION_IP_LOCAL_ADDRESS;
		filter.layerKey = FWPM_LAYER_INBOUND_IPPACKET_V4;
		filter.action.calloutKey = WfpInboundCalloutGUID;
	} else
	{
		condition[0].fieldKey = FWPM_CONDITION_IP_REMOTE_ADDRESS;
		filter.layerKey = FWPM_LAYER_OUTBOUND_IPPACKET_V4;
		filter.action.calloutKey = WfpOutboundCalloutGUID;
	}
	filter.displayData.name = WFP_FILTER_NAME;
	filter.subLayerKey = WfpSublayerGUID;
	filter.weight.type = FWP_UINT64;
	filter.weight.uint64 = &filterWeight;

	filter.numFilterConditions = 1;
	filter.filterCondition = condition;
	filter.action.type = FWP_ACTION_CALLOUT_TERMINATING;

	return FwpmFilterAdd(engineHandle, &filter, NULL, pFilterId);
}
