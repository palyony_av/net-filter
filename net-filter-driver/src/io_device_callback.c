#include <ntddk.h>

#include "include/debug.h"
#include "../src/util/include/ctx.h"
#include "../src/include/wfp_init.h"
#include "../src/include/rule_manager.h"

#include "../src/include/io_device_callback.h"

#define DEVICE_ADD_RULE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_WRITE_DATA)
#define DEVICE_DEL_RULE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_WRITE_DATA)
#define DEVICE_CLEAR_RULES CTL_CODE(FILE_DEVICE_UNKNOWN, 0x803, METHOD_BUFFERED, FILE_WRITE_DATA)

static PDRIVER_CTX g_pDriverCtx = NULL;

VOID InitIoDeviceCallbackGlobals(PDRIVER_CTX pDriverCtx)
{
	g_pDriverCtx = pDriverCtx;
}

NTSTATUS DispatchCreateDevice(IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);

	pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = STATUS_SUCCESS;

	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS DispatchCleanupDevice(IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);

	pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = STATUS_SUCCESS;

	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS DispatchCloseDevice(IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);

	pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = STATUS_SUCCESS;

	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS DispatchReadAVData(IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp)
{
	// Tip: I can store information about deivce in extension structure, but I don't know how.
	// In the artical I can find out https://www.codeproject.com/Articles/8651/A-simple-demo-for-WDM-Driver-development

	UNREFERENCED_PARAMETER(pDeviceObject);
	PTRANSACTION_STRUCT pTransaction = &g_pDriverCtx->transaction;
	PIO_STACK_LOCATION pIoStackLocation = IoGetCurrentIrpStackLocation(pIrp);
	NTSTATUS status = STATUS_SUCCESS;

	ULONG userBufLength = pIoStackLocation->Parameters.Read.Length;
	PVOID userBuf = pIrp->UserBuffer;

	ULONG writedLength = 0;
	if (userBufLength > 0)
	{
		writedLength = TransactionBufferPop((PUCHAR)userBuf, userBufLength, pTransaction);
	}

	pIrp->IoStatus.Information = writedLength;
	pIrp->IoStatus.Status = status;

	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return status;
}

NTSTATUS DispatchControlDevice(IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	PIO_STACK_LOCATION pIoStackLocation = IoGetCurrentIrpStackLocation(pIrp);
	NTSTATUS status = STATUS_SUCCESS;
	ULONG ioControlLength = pIoStackLocation->Parameters.DeviceIoControl.InputBufferLength;
	PVOID ioBuffer = pIrp->AssociatedIrp.SystemBuffer;

	switch (pIoStackLocation->Parameters.DeviceIoControl.IoControlCode)
	{
	case DEVICE_ADD_RULE:
	{
		KdPrint(("%sEnd to log traffic\r\n", DEBUG_CONST));
		//KdPrint(("%.*s\n", (int)ioControlLength, (PCHAR)ioBuffer));
		WfpUnregisterNetworkFilter(&g_pDriverCtx->wfp);

		if (ioControlLength >= sizeof(FILTER_CFG))
		{
			PFILTER_CFG pInRule = (PFILTER_CFG)ioBuffer;
			if (NT_SUCCESS(addRule(pInRule)))
			{
				KdPrint(("%sAdd rule %u\r\n", DEBUG_CONST, pInRule->ruleId));
				TransactionBufferPushA(&g_pDriverCtx->transaction, "[%u] Add rule", pInRule->ruleId);
			} else {
				KdPrint(("%sError during add rule %u\r\n", DEBUG_CONST, pInRule->ruleId));
				TransactionBufferPushA(&g_pDriverCtx->transaction, "[%u] Error during add rule", pInRule->ruleId);
			}
		}		

		status = WfpRegisterNetworkFilter(&g_pDriverCtx->wfp);
		if (!NT_SUCCESS(status))
		{
			break;
		}

		KdPrint(("%sSuccessfully add\r\n", DEBUG_CONST));
	} break;
	case DEVICE_DEL_RULE:
	{
		KdPrint(("%sEnd to log traffic\r\n", DEBUG_CONST));
		WfpUnregisterNetworkFilter(&g_pDriverCtx->wfp);

		if (ioControlLength >= sizeof(FILTER_CFG))
		{
			PDEL_RULE_INFO pDelRuleInfo = (PDEL_RULE_INFO)ioBuffer;
			if (NT_SUCCESS(delRule(pDelRuleInfo)))
			{
				KdPrint(("%sDel rule %u\r\n", DEBUG_CONST, pDelRuleInfo->ruleId));
				TransactionBufferPushA(&g_pDriverCtx->transaction, "[%u]Del rule", pDelRuleInfo->ruleId);
			} else {
				KdPrint(("%sError during del rule %u\r\n", DEBUG_CONST, pDelRuleInfo->ruleId));
				TransactionBufferPushA(&g_pDriverCtx->transaction, "[%u] Error during del rule", pDelRuleInfo->ruleId);
			}
		}

		status = WfpRegisterNetworkFilter(&g_pDriverCtx->wfp);
		if (!NT_SUCCESS(status))
		{
			break;
		}

		KdPrint(("%sSuccessfully delete\r\n", DEBUG_CONST));
	} break;
	case DEVICE_CLEAR_RULES:
	{
		KdPrint(("%sEnd to log traffic\r\n", DEBUG_CONST));
		WfpUnregisterNetworkFilter(&g_pDriverCtx->wfp);

		clearRules();
	
		KdPrint(("%sClear rules\r\n", DEBUG_CONST));
		TransactionBufferPushA(&g_pDriverCtx->transaction, "Clear rules");

		status = WfpRegisterNetworkFilter(&g_pDriverCtx->wfp);
		if (!NT_SUCCESS(status))
		{
			break;
		}

		KdPrint(("%sSuccessfully clear\r\n", DEBUG_CONST));
	} break;
	default:
		break;
	}

	pIrp->IoStatus.Information = ioControlLength;
	pIrp->IoStatus.Status = status;

	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return status;
}
