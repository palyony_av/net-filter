#include "include/debug.h"
#include "../src/packet_processor/include/inbound_procesor.h"
#include "../src/packet_processor/include/outbound_processor.h"

#include "../src/include/wfp_callout_callback.h"

static PDRIVER_WFP_CTX g_pDriverWfpCtx = NULL;
static PTRANSACTION_STRUCT g_pTransaction = NULL;

static void processPacketData(PVOID layerData);

VOID NetFilterInitCallbackGlobals(PDRIVER_WFP_CTX pDriverWfpCtx, PTRANSACTION_STRUCT pTransaction)
{
	g_pDriverWfpCtx = pDriverWfpCtx;
	g_pTransaction = pTransaction;
}

VOID NTAPI NetFilterFilterInboundCallback(_In_ const FWPS_INCOMING_VALUES* inFixedValues,
	_In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues, _Inout_opt_ void* layerData,
	_In_opt_ const void* classifyContext, _In_ const FWPS_FILTER* filter,
	_In_ UINT64 flowContext, _Inout_ FWPS_CLASSIFY_OUT* classifyOut)
{
	UNREFERENCED_PARAMETER(inFixedValues);
	UNREFERENCED_PARAMETER(inMetaValues);
	UNREFERENCED_PARAMETER(classifyContext);
	UNREFERENCED_PARAMETER(flowContext);

	if (!(classifyOut->rights & FWPS_RIGHT_ACTION_WRITE))
	{
		goto End;
	}

	classifyOut->actionType = processInboundIpv4Rules(g_pDriverWfpCtx->pInboundFilterCfg,
		g_pDriverWfpCtx->inboundCounter, layerData, g_pTransaction);

End:
	if (filter->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)
	{
		classifyOut->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
}

VOID NTAPI NetFilterFilterOutboundCallback(_In_ const FWPS_INCOMING_VALUES* inFixedValues,
	_In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues, _Inout_opt_ void* layerData,
	_In_opt_ const void* classifyContext, _In_ const FWPS_FILTER* filter,
	_In_ UINT64 flowContext, _Inout_ FWPS_CLASSIFY_OUT* classifyOut)
{
	UNREFERENCED_PARAMETER(inFixedValues);
	UNREFERENCED_PARAMETER(inMetaValues);
	UNREFERENCED_PARAMETER(classifyContext);
	UNREFERENCED_PARAMETER(flowContext);

	if (!(classifyOut->rights & FWPS_RIGHT_ACTION_WRITE))
	{
		goto End;
	}

	classifyOut->actionType = processOutboundIpRules(g_pDriverWfpCtx->pOutboundFilterCfg,
		g_pDriverWfpCtx->outboundCounter, layerData, g_pTransaction);

End:
	if (filter->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)
	{
		classifyOut->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
}

NTSTATUS NTAPI NetFilterNotifyCallback(
	_In_ FWPS_CALLOUT_NOTIFY_TYPE notifyType,
	_In_ const GUID* filterKey,
	_Inout_ FWPS_FILTER* filter
	)
{
	UNREFERENCED_PARAMETER(notifyType);
	UNREFERENCED_PARAMETER(filterKey);
	UNREFERENCED_PARAMETER(filter);

	return STATUS_SUCCESS;
}

VOID NTAPI NetFilterFlowDeleteCallback(_In_ UINT16 layerId, _In_ UINT32 calloutId, _In_ UINT64 flowContext)
{
	UNREFERENCED_PARAMETER(layerId);
	UNREFERENCED_PARAMETER(calloutId);
	UNREFERENCED_PARAMETER(flowContext);
}

void processPacketData(PVOID layerData)
{
	PNET_BUFFER_LIST pNetList = (PNET_BUFFER_LIST)layerData;

#define TEST_BUFFER_SIZE 30 * 3 + 1
	CHAR test[TEST_BUFFER_SIZE] = { 0 };
	PCHAR testPtr = test;

	KdPrint(("%slayerData %p\n", DEBUG_CONST, layerData));
	if (pNetList != NULL)
	{
		PUINT8 buffer = MmGetMdlVirtualAddress(pNetList->FirstNetBuffer->CurrentMdl);
		ULONG size = MmGetMdlByteCount(pNetList->FirstNetBuffer->CurrentMdl);
		//PUINT8 buffer = (PUINT8)pNetList->FirstNetBuffer->CurrentMdl->MappedSystemVa + pNetList->FirstNetBuffer->CurrentMdlOffset;
		KdPrint(("%sOffset: %lu\n", DEBUG_CONST, pNetList->FirstNetBuffer->CurrentMdlOffset));
		KdPrint(("%sSize: %lu\n", DEBUG_CONST, size));
		KdPrint(("%sLength: %u\n", DEBUG_CONST, pNetList->FirstNetBuffer->DataLength));
		KdPrint(("%sData: ", DEBUG_CONST));

		for (ULONG i = 0; i < 30 && i < size; ++i)
		{
			RtlStringCbPrintfA(testPtr, TEST_BUFFER_SIZE, "%02hhX ", buffer[i]);
			testPtr += 3;
		}
		KdPrint(("%s\n", test));
	}
}
