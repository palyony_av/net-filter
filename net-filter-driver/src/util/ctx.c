#include <ntddk.h>

#include "../util/include/ctx.h"

#define CONTEXT_MEMORY_TAG '1xtc'
#define CONTEXT_WFP_MEMORY_TAG 'wctx'

NTSTATUS ContextCreate(PDRIVER_CTX* ppCtx)
{
	*ppCtx = ExAllocatePoolWithTag(PagedPool, sizeof(DRIVER_CTX), CONTEXT_MEMORY_TAG);
	if (ppCtx == NULL)
	{
		return STATUS_UNSUCCESSFUL;
	}

	RtlZeroMemory(*ppCtx, sizeof(DRIVER_CTX));

	return STATUS_SUCCESS;
}

VOID ContextDelete(PDRIVER_CTX* ppCtx)
{
	if (ppCtx == NULL)
	{
		return;
	}

	PDRIVER_WFP_CTX pDriverWfpCtx = &(*ppCtx)->wfp;
	
	ContextUninitBuffer(pDriverWfpCtx->pFilterName);

	ExFreePoolWithTag(*ppCtx, CONTEXT_MEMORY_TAG);
	*ppCtx = NULL;
}

NTSTATUS ContextInitBuffer(PVOID pData, UINT32 dataSize, PVOID* ppResultBuffer)
{
	PVOID pResultBuffer = NULL;

	if (pData == NULL || ppResultBuffer == NULL)
	{
		return STATUS_UNSUCCESSFUL;
	}

	pResultBuffer = ExAllocatePoolWithTag(PagedPool, dataSize, CONTEXT_WFP_MEMORY_TAG);

	if (pResultBuffer == NULL)
	{
		return STATUS_UNSUCCESSFUL;
	}

	RtlCopyMemory(pResultBuffer, pData, dataSize);

	*ppResultBuffer = pResultBuffer;

	return STATUS_SUCCESS;
}

VOID ContextUninitBuffer(PVOID buffer)
{
	if (buffer != NULL)
	{
		ExFreePoolWithTag(buffer, CONTEXT_WFP_MEMORY_TAG);
	}
}
