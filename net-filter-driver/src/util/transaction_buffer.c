#include <ntddk.h>

#include "../util/include/transaction_buffer.h"

#define TRANSACTION_BUFFER_TAG '1MBT'  // Transaction Buffer Memory 1

NTSTATUS TransactionBufferInit(PTRANSACTION_STRUCT pTransaction, USHORT bufferSize)
{
	UNICODE_STRING transactionBuffer = { 0 };
	PWCHAR pBuffer = NULL;

	if (pTransaction == NULL)
	{
		return STATUS_UNSUCCESSFUL;
	}

	pBuffer = ExAllocatePoolWithTag(PagedPool, bufferSize, TRANSACTION_BUFFER_TAG);
	
	if (pBuffer == NULL)
	{
		return STATUS_UNSUCCESSFUL;
	}

	RtlInitEmptyUnicodeString(&transactionBuffer, pBuffer, bufferSize);

	KeInitializeSpinLock(&pTransaction->irpTransactionSpinLock);
	pTransaction->buffer = transactionBuffer;

	return STATUS_SUCCESS;
}

VOID TransactionBufferDelete(PTRANSACTION_STRUCT pTransaction)
{
	if (pTransaction == NULL || pTransaction->buffer.Buffer == NULL)
	{
		return;
	}

	ExFreePoolWithTag(pTransaction->buffer.Buffer, TRANSACTION_BUFFER_TAG);
	
	pTransaction->buffer.Length = 0;
	pTransaction->buffer.MaximumLength = 0;
	pTransaction->buffer.Buffer = NULL;
}

ULONG TransactionBufferPop(PVOID pBuffer, ULONG size, PTRANSACTION_STRUCT pTransaction)
{
	ULONG safeBufferSize = 0;
	PKSPIN_LOCK pIrpTransactionSpinLock = NULL;
	KIRQL  oldIrql = 0;

	if (pTransaction == NULL || pTransaction->buffer.Buffer == NULL || pBuffer == NULL)
	{
		return 0;
	}

	pIrpTransactionSpinLock = &pTransaction->irpTransactionSpinLock;
	
	KeAcquireSpinLock(pIrpTransactionSpinLock, &oldIrql);

	safeBufferSize = min(pTransaction->buffer.Length, size);

	RtlCopyMemory(pBuffer, pTransaction->buffer.Buffer, safeBufferSize);
	pTransaction->buffer.Length = 0;

	KeReleaseSpinLock(pIrpTransactionSpinLock, oldIrql);

	return safeBufferSize;
}

VOID TransactionBufferReset(PTRANSACTION_STRUCT pTransaction)
{
	PKSPIN_LOCK pIrpTransactionSpinLock = NULL;
	KIRQL  oldIrql = 0;

	if (pTransaction == NULL)
	{
		return;
	}

	pIrpTransactionSpinLock = &pTransaction->irpTransactionSpinLock;

	KeAcquireSpinLock(pIrpTransactionSpinLock, &oldIrql);

	pTransaction->buffer.Length = 0;

	KeReleaseSpinLock(pIrpTransactionSpinLock, oldIrql);
}
