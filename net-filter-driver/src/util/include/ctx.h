#ifndef __CTX_H__
#define __CTX_H__

#include <ntddk.h>
#include <inaddr.h>

#include "transaction_buffer.h"

#define INBOUND_DIRECTION 0
#define OUTBOUND_DIRECTION 1

#define MAX_FLT_CONFIGS 100

typedef enum
{
	IP_PROTOCOL = 0, // Used for all ip packets 
	ICMP_PROTOCOL = 1,
	TCP_PROTOCOL = 6,
	UDP_PROTOCOL = 17
} TRANSPORT_PROTOCOL_ENUM, *PTRANSPORT_PROTOCOL_ENUM;

typedef enum
{
	IPV4_LOG_TRAFFIC,
	IPV4_BLOCK_TRAFFIC
} TRAFFIC_ACTION_ENUM, *PTRAFFIC_ACTION_ENUM;

#pragma pack(push, 1)

#pragma warning(push)
#pragma warning(disable:4201)       // unnamed struct/union

// TODO: Need to remove unused
typedef struct
{
	union
	{
		UINT32 flag;
		struct
		{
			UINT32 direction : 1;
			UINT32 tProtocol : 8;	// Transport protocol
			UINT32 useBytePattern : 1;
			UINT32 unused : 22;
		} bit;
	};
} PARAMETERS_FLAG, * PPARAMETERS_FLAG;

#pragma warning(pop)

typedef union
{
	struct
	{
		UINT16 offset;
		UINT16 length;	// The user can directly set it
	} param;
	UINT32 value;
} PATTERN_PARAM, * PPATTERN_PARAM;

typedef struct
{
	IN_ADDR addr;
	UINT32 mask;
	union
	{
		struct
		{
			UINT16 start;
			UINT16 end;
		} range;
		UINT32 value;
	} port;

} IPV4_RULE, *PIPV4_RULE;

typedef struct
{
	UINT8 ruleId;
	PARAMETERS_FLAG parameters;
	IPV4_RULE remote;
	IPV4_RULE local;

	PATTERN_PARAM pattern;

	PUINT8 patternData;

	TRAFFIC_ACTION_ENUM action;
} FILTER_CFG, *PFILTER_CFG;

typedef struct
{
	UINT32 regInboundCalloutId;
	UINT32 regOutboundCalloutId;
	UINT32 addInboundCalloutId;
	UINT32 addOutboundCalloutId;
	HANDLE engineHandle;
	PWCHAR pFilterName;

	UINT64 inboundFilterId;
	UINT64 outboundFilterId;

	UINT8 inboundCounter;
	UINT8 outboundCounter;
	PFILTER_CFG pInboundFilterCfg;
	PFILTER_CFG pOutboundFilterCfg;
} DRIVER_WFP_CTX, *PDRIVER_WFP_CTX;

typedef struct
{
	PDEVICE_OBJECT pWdfDeviceObject;
	HANDLE wdfRegParametersKey;
	PDEVICE_OBJECT pIoDeviceObject;						// Init in DriverEntry | Used in: Unload;
	TRANSACTION_STRUCT transaction;
	DRIVER_WFP_CTX wfp;
}  DRIVER_CTX, * PDRIVER_CTX;

#pragma pack(pop)

NTSTATUS ContextCreate(PDRIVER_CTX* ppCtx);
NTSTATUS ContextInitBuffer(PVOID pData, UINT32 dataSize, PVOID* ppResultBuffer);
VOID ContextDelete(PDRIVER_CTX* ppCtx);
VOID ContextUninitBuffer(PVOID buffer);

#endif // !__CTX_H__
