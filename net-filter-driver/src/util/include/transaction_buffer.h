#ifndef __TRANSACTION_BUFFER__
#define __TRANSACTION_BUFFER__

#include <ntstrsafe.h>
#include <inaddr.h>

#define DEFAULT_TRANSACTION_BUFFER_SIZE 2048
#define DEFAULT_MAX_TRANSACTION_BUFFER_SYMBOLS (DEFAULT_TRANSACTION_BUFFER_SIZE >> 1)
#define UNKNOWN_MODULE_ID -1

typedef struct
{
	KSPIN_LOCK irpTransactionSpinLock;
	UNICODE_STRING buffer;
} TRANSACTION_STRUCT, * PTRANSACTION_STRUCT;

NTSTATUS TransactionBufferInit(PTRANSACTION_STRUCT pTransaction, USHORT bufferSize);
VOID TransactionBufferDelete(PTRANSACTION_STRUCT pTransaction);
ULONG TransactionBufferPop(PVOID pBuffer, ULONG size, PTRANSACTION_STRUCT pTransaction);
VOID TransactionBufferReset(PTRANSACTION_STRUCT pTransaction);

#define TransactionBufferPushA(_pTransaction, _fmt, ...)	{ \
NTSTATUS __transactionStatus = 0; \
PCHAR __pTransactionBuffer = (PCHAR)(_pTransaction)->buffer.Buffer; \
USHORT __pTransactionLength = (_pTransaction)->buffer.Length; \
USHORT __pTransactionMaximumLength = (_pTransaction)->buffer.MaximumLength; \
PCHAR __remainTransactionBuffer = NULL; \
USHORT __remainTransactionLength = 0; \
USHORT __remainTransactionMaximumLength = 0; \
LARGE_INTEGER __eventTime; \
TIME_FIELDS __eventTimeFields; \
KIRQL  oldIrql; \
PKSPIN_LOCK pIrpTransactionSpinLock = &(_pTransaction)->irpTransactionSpinLock; \
\
KeQuerySystemTime(&__eventTime); \
ExSystemTimeToLocalTime(&__eventTime, &__eventTime); \
(VOID)RtlTimeToTimeFields( &__eventTime, &__eventTimeFields); \
\
KeAcquireSpinLock(pIrpTransactionSpinLock, &oldIrql);  \
\
__remainTransactionBuffer = &(__pTransactionBuffer)[__pTransactionLength / sizeof(CHAR)]; \
__remainTransactionLength = 0; \
__remainTransactionMaximumLength = __pTransactionMaximumLength - __pTransactionLength; \
\
__transactionStatus = RtlStringCbPrintfA((char *)__remainTransactionBuffer, __remainTransactionMaximumLength, \
	"{\"id\":0,\"module_id\":6,\"event\":\"" _fmt "\",\"date\":\"%u-%u-%u\",\"time\":\"%u:%u:%u\"}\n", __VA_ARGS__, \
	(USHORT)__eventTimeFields.Day, (USHORT)__eventTimeFields.Month, (USHORT)__eventTimeFields.Year, \
	(USHORT)__eventTimeFields.Hour, (USHORT)__eventTimeFields.Minute, (USHORT)__eventTimeFields.Second); \
if (NT_SUCCESS(__transactionStatus)) \
{ \
char *pFindedChar = strchr((char *)__remainTransactionBuffer, '\0'); \
__pTransactionLength += (USHORT)((char *)pFindedChar - (char *)__remainTransactionBuffer); \
} \
(_pTransaction)->buffer.Buffer = (PWCH)__pTransactionBuffer; \
(_pTransaction)->buffer.Length = __pTransactionLength; \
(_pTransaction)->buffer.MaximumLength = __pTransactionMaximumLength; \
\
KeReleaseSpinLock(pIrpTransactionSpinLock, oldIrql); \
}

#endif // !__TRANSACTION_BUFFER__

//__transactionStatus = RtlUnicodeStringPrintf(&__remainTransactionBuffer, \
//	L"{\"module_id\":6,\"event\":\"" _fmt L"\",\"date\":\"%u-%u-%u\",\"time\":\"%u:%u:%u\"}\n", __VA_ARGS__, \
//	(USHORT)__eventTimeFields.Day, (USHORT)__eventTimeFields.Month, (USHORT)__eventTimeFields.Year, \
//	(USHORT)__eventTimeFields.Hour, (USHORT)__eventTimeFields.Minute, (USHORT)__eventTimeFields.Second); \
