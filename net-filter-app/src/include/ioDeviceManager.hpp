#ifndef __IODEVICEMANAGER_HPP__
#define __IODEVICEMANAGER_HPP__

#include <Windows.h>
#include <memory>

#include "rule.h"

#define DEVICE_ADD_RULE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_WRITE_DATA)
#define DEVICE_DEL_RULE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_WRITE_DATA)

class IoDeviceManager
{
public:
    static std::unique_ptr<IoDeviceManager> make(const wchar_t * pIoDeviceName);
    bool addRule(PFILTER_CFG pCfg);
    bool delRule(PDEL_RULE_INFO pDelRuleInfo);
    DWORD pullLogs(void* buf, DWORD bufSize);
    ~IoDeviceManager();

private:
    IoDeviceManager(HANDLE hIoDevice);

    HANDLE hIoDevice = nullptr;
};

inline std::unique_ptr<IoDeviceManager> IoDeviceManager::make(const wchar_t * pIoDeviceName)
{
    HANDLE deviceHandle = CreateFile(pIoDeviceName, GENERIC_ALL, 0, 0,
        OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM, 0);

	if (deviceHandle == INVALID_HANDLE_VALUE || deviceHandle == NULL)
	{
		return nullptr;
	}

    return std::unique_ptr<IoDeviceManager>(new IoDeviceManager(deviceHandle));
}

IoDeviceManager::IoDeviceManager(HANDLE hIoDevice)
:hIoDevice(hIoDevice)
{

}

IoDeviceManager::~IoDeviceManager()
{
    CloseHandle(IoDeviceManager::hIoDevice);
}

bool IoDeviceManager::addRule(PFILTER_CFG pCfg)
{
    if (pCfg == nullptr)
    {
        return false;
    }

    DWORD size = sizeof(FILTER_CFG) + pCfg->pattern.param.length;

    if (!DeviceIoControl(IoDeviceManager::hIoDevice, DEVICE_ADD_RULE, (void *)pCfg, size, NULL, 0, NULL, NULL))
	{
		return false;
	}

    return true;
}

bool IoDeviceManager::delRule(PDEL_RULE_INFO pDelRuleInfo)
{
    if (pDelRuleInfo == nullptr)
    {
        return false;
    }

    DWORD size = sizeof(PDEL_RULE_INFO);

    if (!DeviceIoControl(IoDeviceManager::hIoDevice, DEVICE_DEL_RULE, (void *)pDelRuleInfo, size, NULL, 0, NULL, NULL))
	{
		return false;
	}

    return true;
}

inline DWORD IoDeviceManager::pullLogs(void* buf, DWORD bufSize)
{
    DWORD readedBytes = 0;

    if (!ReadFile(IoDeviceManager::hIoDevice, buf, bufSize, &readedBytes, NULL))
    {
        std::cout << "Bad Read file" << std::endl;
        return 0;
    }

    return readedBytes;
}

#endif
