#ifndef __PIPEMANAGER_HPP__
#define __PIPEMANAGER_HPP__

#include <Windows.h>
#include <memory>
#include <cstdint>
#include <tuple>

class PipeManager
{
public:
	~PipeManager();
    std::tuple<char *, size_t> readServerMsg();
    void writeToServer(wchar_t *buf, DWORD fillSize);
    static std::unique_ptr<PipeManager> make(const wchar_t* pipeName, DWORD dwDesiredAccess);

private:
    PipeManager(HANDLE hPipe, uint32_t readBufCapacity = 1024);
    char *pBuf;
    uint32_t bufSize;
    HANDLE hPipe = NULL;
};

PipeManager::PipeManager(HANDLE hPipe, uint32_t readBufCapacity)
:hPipe(hPipe)
{
    PipeManager::pBuf = new char[readBufCapacity];
    bufSize = readBufCapacity;    
}

inline std::unique_ptr<PipeManager> PipeManager::make(const wchar_t* pipeName, DWORD dwDesiredAccess)
{
    HANDLE hPipe;

    hPipe = CreateFile(pipeName,
        dwDesiredAccess,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);

    if (hPipe == INVALID_HANDLE_VALUE)
    {
        return std::unique_ptr<PipeManager>();
    }

    return std::unique_ptr<PipeManager>(new PipeManager(hPipe));
}

PipeManager::~PipeManager()
{
    CloseHandle(PipeManager::hPipe);
    delete[] PipeManager::pBuf;
}

inline std::tuple<char *, size_t> PipeManager::readServerMsg()
{
    size_t dwReaded = 0;

    ReadFile(hPipe,
            PipeManager::pBuf,
            PipeManager::bufSize,   // = length of string + terminating '\0' !!!
            (DWORD *)&dwReaded,
            NULL);

    if (0 < dwReaded && dwReaded < PipeManager::bufSize)
    {
        PipeManager::pBuf[dwReaded] = 0;
    }

    return std::tuple<char *, size_t>(PipeManager::pBuf, dwReaded);
}

inline void PipeManager::writeToServer(wchar_t* szBuf, DWORD bytesToWrite)
{
    const wchar_t* pBufLastChr = &szBuf[(bytesToWrite / sizeof(wchar_t)) - 1];
    wchar_t* pBufStart = szBuf;
    wchar_t* pBufEnd = nullptr;
    
    if (bytesToWrite == 0)
    {
        return;
    }

    pBufEnd = std::wcschr(pBufStart, '\n');

    while (pBufEnd != nullptr && pBufEnd < pBufLastChr)
    {
        DWORD sizeItemToWrite = (DWORD)((char *)pBufEnd - (char *)pBufStart);
        std::cout << sizeItemToWrite << std::endl;
        DWORD written = 0;
        /* Need to add error checking, but now I don't care */
        WriteFile(hPipe, pBufStart, sizeItemToWrite, &written, NULL);
        pBufStart = &pBufEnd[1];
        pBufEnd = std::wcschr(pBufStart, '\n');
    }

    if (pBufEnd != nullptr)
    {
        DWORD sizeItemToWrite = (DWORD)((char*)pBufEnd - (char*)pBufStart);
        DWORD written = 0;
        WriteFile(hPipe, pBufStart, sizeItemToWrite, &written, NULL);
    }
}

#endif // !__PIPEMANAGER_HPP__
